import React, { Component } from 'react'
import { renderTextField, renderSelect, renderTextArea } from './form-elements/FormElements'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import FooterPanel from './Footer'
import { FormattedMessage } from 'react-intl';


import { changeStatus, getExternalState, getTicketState } from '../middlewares/bulkActionMiddleware'


class ChangeStatus extends Component {

  constructor(props) {
    super(props)
  }
  componentDidMount() {
    const { onGetExternalState } = this.props
    onGetExternalState;
  }
  render() {

    return <ChangeStatusForm onSubmit={this.props.handleSubmit} externalStateList={this.props.externalStateList} ticket_status={this.props.ticket_status} />
  }
}
let ChangeStatusForm = props => {
  const { handleSubmit, externalStateList, ticket_status } = props;
  var options = [];
  options.push(<FormattedMessage id='app.select_state' defaultMessage={"Select State"}>
    {(message) => <option value="" key="011">{message}</option>}
  </FormattedMessage>)
  if (externalStateList.externalStates) {
    var list = externalStateList.externalStates;
    Object.keys(list).forEach((key) => {
      //console.log('key===',key);
      if (key !== 'CLOSED') {
        var otherState = list[key];
        for (let i = 0; i < list[key].length; i++) {

          options.push(<option value={otherState[i]} key={i}>{otherState[i]}</option>)
        }
      }

    })
    /*for(let i=0;i<list.length;i++){
     
      options.push(<option value={list[i]} key={i}>{list[i]}</option>)
    }*/
  }
  return (
    <form onSubmit={handleSubmit}>
      <div className="bulk-action">
        <div className="bulk-action__content">
          <div id="ticketState" className="row" >
            <div className="col s12 no-padding">
              <div className="row select-no-padding">

                <Field name="ticket_status" component={renderSelect}
                  label={<FormattedMessage
                    id='app.change_status'
                    defaultMessage={"Change Status"}
                  />} s={6} m={6}>
                  {options}
                </Field>


              </div>

            </div>
          </div>

        </div>
        <FooterPanel enabledApplyButton={ticket_status} source={"Change Status"} />
      </div>
    </form>
  )
}

ChangeStatusForm = reduxForm({
  form: 'form1'                 // <------ same form name
})(ChangeStatusForm)

ChangeStatus = connect(
  state => ({
    ticket_status: state.form.form1 !== undefined ? (state.form.form1.values !== undefined ?
      (state.form.form1.values.ticket_status !== undefined ?
        (state.form.form1.values.ticket_status !== " " ? state.form.form1.values.ticket_status :
          false) : false) : false) : false

  }))(ChangeStatus)

const mapStateToProps = (state, ownProps) => {
  return {
    externalStateList: getTicketState(state)
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  var updateStatus = (values) => {
    dispatch(changeStatus(values));
  }

  return {
    onGetExternalState: dispatch(getExternalState()),
    handleSubmit: updateStatus

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ChangeStatus)





