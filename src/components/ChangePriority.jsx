import React, { Component } from 'react'
import {renderRadioButton} from './form-elements/FormElements'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import FooterPanel from './Footer'
import {FormattedMessage} from 'react-intl';


import {changeTicketPriority} from '../middlewares/bulkActionMiddleware'


class ChangePriority extends Component {

  constructor(props) {
    super(props) 
  }
  componentWillMount(){
  
  }
 render() {
      
          return <ChangePriorityForm onSubmit={this.props.handleSubmit} loader={this.props.loader} />
  }
}
let ChangePriorityForm=props => {
  const{handleSubmit,loader}=props;
  var enabledApplyButton = true;
  return(
  <form onSubmit={handleSubmit}>
  <div className="bulk-action">
  <div className="bulk-action__content">
    <div id="ticketPriority" className="row">
 <div className="col s12 m12 no-padding">
            <div className="row">
                                        <div className="col s12 m12 no-padding">
                                                <h4><FormattedMessage
                  id='app.change_priority'
                  defaultMessage={"Change Priority to:"}
                /></h4>
                                        </div>
                                </div>
                                <div className="row">
                                        <div className="col s12 no-padding">
                                          
                                            <Field name="ticket_priority" id="high" className="with-gap" component={renderRadioButton} type="radio" value="High" label={<FormattedMessage
                  id='app.high'
                  defaultMessage={"High"}
                                            /> }/>
                                                
                                           
                                        </div>
                                        <div className="col s12 no-padding">
                                          
                                               <Field name="ticket_priority" id="medium" className="with-gap" component={renderRadioButton} type="radio" value="Medium" label={<FormattedMessage
                  id='app.medium'
                  defaultMessage={"Medium"}
                />}/>
                                                
                                         
                                        </div>
                                        <div className="col s12 no-padding">
                                          
                                                <Field name="ticket_priority" id="low" className="with-gap" component={renderRadioButton} type="radio" value="Low" label={<FormattedMessage
                  id='app.low'
                  defaultMessage={"Low"}
                />}/>
                                               
                                         
                                        </div>
                                </div>
                        </div>
                </div>
    
  </div>
  <FooterPanel enabledApplyButton = {enabledApplyButton} source="ChangePriority"/>
  

  </div>

</form>
   )
}

ChangePriorityForm= reduxForm({
  form: 'priorityform',                 // <------ same form name
  initialValues:{ticket_priority:'High'}
  
  })(ChangePriorityForm)

const mapStateToProps = (state, ownProps) => {
  return {
      loader:(state.uiSelection !== undefined) ? (state.uiSelection.loader !==undefined ? state.uiSelection.loader : undefined) : undefined
   }
}
const mapDispatchToProps = (dispatch, ownProps) => {
   var updatePriority=(values)=>{
        dispatch(changeTicketPriority(values));
  }
  
 

 
  return {
    handleSubmit:updatePriority
    
  }
}
 export default connect(mapStateToProps,mapDispatchToProps)(ChangePriority)

 



