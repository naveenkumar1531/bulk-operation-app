import React from 'react'
import {Input,Row,Autocomplete} from 'react-materialize'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { DatePicker, TimePicker,  Toggle} from 'redux-form-material-ui'


import { Field, FieldArray,reduxForm } from 'redux-form'
import {
  renderRadioButton,
  renderSelect,
  renderTextArea,
  renderTextField,
  renderTextFieldInteger,
  renderSelectMulti,
  renderCheckBox
} from './FormElements'

import { DecimalValue, IntegerValue } from './FormValidation'


const DateField = ({selectedCustomFieldObj}) =>{
  var customFieldName= selectedCustomFieldObj['customFieldName'];
  var customFieldId= selectedCustomFieldObj['customFieldId'];
  var customFieldDataType= selectedCustomFieldObj['customFieldDataType'];
  return(
    <div className="col s6 m6 no-padding ">
    <Field  name={customFieldName} component={DatePicker} format={null} hintText={customFieldName} />
        </div>
  );
}

const SingleLineField=({selectedCustomFieldObj})=>{
  var customFieldName= selectedCustomFieldObj['customFieldName'];
  var customFieldId= selectedCustomFieldObj['customFieldId'];
  var customFieldDataType= selectedCustomFieldObj['customFieldDataType'];
  if (customFieldDataType=='Integer' ){
  return(
    <div className="col s6 m6 no-padding ">
    <div className="input-field">
    <Field id={customFieldId} name={customFieldId} normalize={IntegerValue}
           component={renderTextFieldInteger} style={{ height: "150px "}} label={customFieldName} />
           </div>
           </div>
  )
}else if (customFieldDataType=='Decimal'){
  return(
    <div className="col s6 m6 no-padding ">
    <div className="input-field">
    <Field id={customFieldId} name={customFieldId} normalize={DecimalValue}
           component={renderTextFieldInteger} style={{ height: "150px "}} label={customFieldName} />
           </div>
           </div>
  )
}else {
  return(
    <div className="col s6 m6 no-padding ">
      <div className="input-field">
            <Field id={customFieldId} name={customFieldId}
             component={renderTextField} label={customFieldName} />
             <label htmlFor={customFieldName} className="active">{customFieldName}</label>
      </div>
    </div>
  );
}
}
  const MultiLineField=({selectedCustomFieldObj})=>{
    var customFieldName =  selectedCustomFieldObj['customFieldName'];
    var customFieldId   =    selectedCustomFieldObj['customFieldId'];
      return (
            <div className="col s12 m12 no-padding ">
              <div className="input-field">
                <Field id={customFieldId} name={customFieldId} rows ="4" className="materialize-textarea"
             component={renderTextArea} />
             <label htmlFor={customFieldName} className="active">{customFieldName}</label>
              </div>
            </div> 
          );

}

 
          var checkBoxList=[];
  const CheckBox=({selectedCustomFieldObj})=>{
   
    console.log("check box button",selectedCustomFieldObj);
    var customFieldName= selectedCustomFieldObj['customFieldName'];
    var customFieldId= selectedCustomFieldObj['customFieldId'];
    var dataType= selectedCustomFieldObj['customFieldDataType'];
    var possibleValues  = selectedCustomFieldObj['possibleValues'];
    var checkBoxList=[];
    for (var values in possibleValues){
        var fieldValue= (possibleValues[values]).toString();
        checkBoxList.push(
           <Field id={fieldValue} className="filled-in takeAction" name={fieldValue}
           component={renderCheckBox} label={fieldValue} />
          )
    }
    return (
      <div>
        {checkBoxList}
      </div>   
      )

    }
  const RadioButton=({selectedCustomFieldObj})=>{
    console.log("radio button",selectedCustomFieldObj);
    var customFieldName= selectedCustomFieldObj['customFieldName'];
    var customFieldId= selectedCustomFieldObj['customFieldId'];
    var possibleValues  = selectedCustomFieldObj['possibleValues']  ;
    var radioLabel=[];
    for (var values in possibleValues){
      var fieldValue= (possibleValues[values]).toString();
        radioLabel.push(
                <div class="col s2 no-padding">
                  <Field name={customFieldId}  className="with-gap" component={renderRadioButton} type="radio" value={fieldValue} label={fieldValue} />        
                </div>);
      }
      console.log("radioLabel",radioLabel);
    return ( <div>
              {radioLabel}
              </div>
           )
  }
  const SingleSelectionListBox=({selectedCustomFieldObj})=>{
    var customFieldName= selectedCustomFieldObj['customFieldName'];
    var customFieldId= selectedCustomFieldObj['customFieldId'];
    var possibleValues  = selectedCustomFieldObj['possibleValues']  ;
    var options=[];
    options.push(
                  <option>  </option>);
    for (var values in possibleValues){
        options.push(
                  <option>{possibleValues[values]}</option>
                  
        );
      }
    return ( 
            <div className="col s12 no-padding"> 
                <div className="row select">
                  <Field name={customFieldId}  component={renderSelect}  label={customFieldName} s={6} m={6}>
                    {options}
                  </Field>
                </div>
              </div>
          );
}
  const MultiSelectionListBox=({selectedCustomFieldObj})=>{
    var customFieldName= selectedCustomFieldObj['customFieldName'];
    var customFieldId= selectedCustomFieldObj['customFieldId'];
    var possibleValues  = selectedCustomFieldObj['possibleValues']  ;
    var options=[];
    for (var values in possibleValues){
        options.push(
                  <option>{possibleValues[values]}</option>                  
        );
      }
    return ( 
            <div className="col s12 no-padding"> 
                <div className="row select">
                  <Field name={customFieldId}  component={renderSelectMulti}  label={customFieldName} s={6} m={6}>
                    {options}
                  </Field>
                </div>
              </div>
          );
}
var Options2=[];
  const dependentField=(selectedValue,completeObj)=>{
    //console.log("hello",selectedValue,"completeObj",completeObj);
     Options2.push(<option value="name"  
                     key="4" >     Gaurav   </option>); 
                      Options2.push(<option value="name"  
                     key="1" >     Gaurav   </option>);  Options2.push(<option value="name"  
                     key="2" >     Gaurav   </option>);  Options2.push(<option value="name"  
                     key="3" >     Gaurav   </option>); 
    return(
         <div className="col s12 no-padding"> 
                <div className="row select">
                  <Field name="gaurav"  component={renderSelect}  label="gaurav" s={6} m={6}>
                    {Options2}
                  </Field>
                </div>
              </div>)
  }
  const FirstLabel=({selectedCustomFieldObj})=>{
      var Options1=[];
      Object.keys(selectedCustomFieldObj).forEach((key) => { 
        Options1.push(<option value={selectedCustomFieldObj[key].name}  
                     key={selectedCustomFieldObj[key].name} >{selectedCustomFieldObj[key].name} 
        </option>);                     
      }); 
      
      return(
      <div>
      <Field name="campaigdddnId" 
              component={renderSelect} s={6} l={6} m={6} onChange={(e) => { dependentField(e.target.value,selectedCustomFieldObj) }} > 
                {Options1} 
        </Field> 
        <Field name="campaign" 
              component={renderSelect} s={6} l={6} m={6} > 
                {Options2} 
        </Field> 
        </div>
        );
  }

  const DependentSingleSelectionListBox=({selectedCustomFieldObj})=>{
    /*var customFieldName= selectedCustomFieldObj['customFieldName'];
    var customFieldId= selectedCustomFieldObj['customFieldId'];
    var possibleValues  = selectedCustomFieldObj['possibleValues'];
    var possibleValueList = possibleValues['value']['possibleValueList'];
    console.log("possibleValueList=",possibleValueList);
    return ( <FirstLabel selectedCustomFieldObj={possibleValueList}   />
    );
    */ 
  }
  const DefaultField=({customFieldId,customFieldName})=>{
    return ( <div className="row"><div className="col s12 m12 no-padding"><div className="input-field"><Field name={customFieldId} id={customFieldId} className="materialize-textarea" component={renderTextArea} /> <label htmlFor={customFieldId} className="active">{customFieldName}</label></div></div></div>);

}

module.exports = {
  SingleLineField,
  MultiLineField,
  CheckBox,
  RadioButton,
  SingleSelectionListBox,
  MultiSelectionListBox,
  DependentSingleSelectionListBox,
  dependentField,
  DateField,
  DefaultField
}