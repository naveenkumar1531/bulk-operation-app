var successTicketArray = [];
var failureTicketArray = [];

function invokeAsBatch(action = '', batchSize, dataArray = [], actionData, type, resolve, reject) {
    //return new Promise(function(resolve, reject) {
    successTicketArray = [];
    failureTicketArray = [];
    var totalArray = dataArray.length;
    chunkArray = splitArrayIntoChunk(batchSize, dataArray);
    if (type === 'action') {
        bulkActionUpdate(action, batchSize, chunkArray, totalArray, actionData, Index = 0, resolve, reject);
    } else {
        bulkUpdateData(batchSize, chunkArray, totalArray, actionData, Index = 0, resolve, reject);
    }

    //}). catch (reject(failureTicketArray))

}

function bulkActionUpdate(action, batchSize, chunkArray, totalArray, actionData, Index, resolve, reject) {
    var client = window.AmeyoClient.init();
    client.contextData.bulkAction("ticket", action, chunkArray[Index], actionData)
        .then(
            function(successData) {
                successTicketArray.push(successData);
                if (Index < chunkArray.length - 1) {
                    Index = Index + 1;
                    bulkActionUpdate(action, batchSize, chunkArray, totalArray, actionData, Index, resolve, reject)
                } else {
                    resolve(totalArray,successTicketArray)
                   
                }
            }
        )
        .catch(
            function(failureData) {
            	Object.keys(failureData).forEach((key)=>{
            		failureTicketArray.push(failureData[key]);	
            	})
                if (Index < chunkArray.length - 1) {
                    Index = Index + 1;
                    bulkActionUpdate(action, batchSize, chunkArray, totalArray, actionData, Index, resolve, reject)
                } else {
                    reject(totalArray, failureTicketArray)
                }
            }
        );
}


function bulkUpdateData(batchSize, chunkArray, totalArray, actionData, Index, resolve, reject) {
    var client = window.AmeyoClient.init();
    client.contextData.bulkUpdate("ticket", chunkArray[Index], actionData)
        .then(
            function(successData) {
                successTicketArray.push(successData);
                if (Index < chunkArray.length - 1) {
                    Index = Index + 1;
                    bulkUpdateData(batchSize, chunkArray, totalArray, actionData, Index, resolve, reject)
                } else {
                    resolve(totalArray,successTicketArray)
                    if (failureTicketArray.length > 0) {
                        reject(totalArray, failureTicketArray)
                    }
                }
            }
        )
        .catch(
            function(failureData) {
                failureTicketArray.push(failureData);
                if (Index < chunkArray.length - 1) {
                    Index = Index + 1;
                    bulkUpdateData(batchSize, chunkArray, totalArray, actionData, Index, resolve, reject)
                } else {
                    try {
                        if (successTicketArray.length > 0) {
                            resolve(totalArray,successTicketArray);
                        }
                    } catch (e) {

                    }
                    reject(totalArray, failureTicketArray)
                }
            }
        );
}



function splitArrayIntoChunk(batchSize, dataArray) {
    var row = []
    if (batchSize >= dataArray.length) {
        row.push(dataArray)
    } else {
        var arrayLen = dataArray.length;
        for (let i = 0; i < arrayLen; i += batchSize) {
            row.push(dataArray.slice(i, i + batchSize))
        }

    }
    return row
}

function extractTicketId(ticketObject) {
    var array = []
    for(var i=0; i<ticketObject.length;i++){
    //Object.keys(ticketObject).forEach((key) => {
        array.push(ticketObject[i].ticketId)
    //})
    }
    return array;

}

function getBatchSize() {
    return 2;
}