function getIframeUrl() {

	var str = window.location;
	var url = str.protocol + "//" + str.host + str.pathname;
	return url;

}

function getDefaultRoute() {

	return '/';
}

function errorMessagesList() {
	var message = { "101": "internal failure", "102": "internal failure", "103": "mandatory field not provided or resource not allocated", "121": "internal failure", "122": "internal failure", "123": "internal failure", "124": "provided ticket id not selected", "125": "ticket not assigned to user ", "126": "provided ticket id already assigned to logged in user", "127": "provided ticket already in the assigned state" }
	return message;
}

function errorMessageGroup() {
	var errorCodeGroup = [101, 102, 121, 122, 123];
	return errorCodeGroup;
}



function getParameterByName(name) {
	var url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex
			.exec(url);
	if (!results) {
		return null;
	}
	if (!results[2]) {
		return '';
	}
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getDefaultLocale() {
	var locale = window.getParameterByName('locale');
	//var locale = undefined;
	if (locale === undefined || locale === null) {
		return 'en';
	}
	// locale='ja';
	return locale;

}

function getTranslations() {
	var locale = getParameterByName('locale');
	if (locale === null) {
		locale = 'en';
	}
	var messages = {   //english translations
		"app.change_priority": "Change Priority to:",
		"app.high": "High",
		"app.medium": "Medium",
		"app.low": "Low",
		"app.select_State": "Select State",
		"app.change_Escalation": "Change Escalation to:",
		"app.escalate": "Escalate",
		"app.de_escalate": "De-escalate",
		"app.closure_Comment": "Closure Comment",
		"app.messagejsx_header": "You have selected tickets from multiple campaigns",
		"app.messagejsx_para": "Custom field can be updated if you select tickets from the same campaign",
		"app.update_ticket_html": "Selected Field Type Not Supported by this App",
		"app.select_Custom_Field": "Select Custom Field:",
		"app.more": "More",
		"app.back": "Back",
		"app.apply": "Apply",
		"title": "More Options",
		"app.close_ticket": "Close Ticket",
		"app.change_priority": "Change Priority",
		"app.change_status": "Change Status",
		"app.esclation": "Esclation",
		"app.custom_field_update": "Custom Field Update",
		"app.pick_ticket": "Pick Ticket",
		"app.ticket_selected": "Ticket Selected",
		"app.close_state": "Close State",
		"app.unassigned_can_be_picked": "Only the tickets that are unassigned can be picked",
		"app.ticket": "Ticket",
		"app.tickets": "Tickets",
		"app.assgined_to_you": "wiil be assigned to you",
		"app.select_state": "Select State",
		"app.selectOption":"Select Option"

	}
	switch (locale) {
		case 'ja':    //Japanese Translations
			messages = {
				"app.change_priority": "JP_Change_Priority_to:",
				"app.high": "JP_High",
				"app.medium": "JP_Medium",
				"app.low": "JP_Low",
				"app.select_State": "JP_Select_State",
				"app.change_Escalation": "JP_Change_Escalation_to:",
				"app.escalate": "JP_Escalate",
				"app.de_escalate": "JP_De-escalate",
				"app.closure_Comment": "JP_Closure_Comment",
				"app.messagejsx_header": "JP_You have selected tickets from multiple campaigns",
				"app.messagejsx_para": "JP_Custom field can be updated if you select tickets from the same campaign",
				"app.update_ticket_html": "JP_Selected Field Type Not Supported by this App",
				"app.select_Custom_Field": "JP_Select Custom Field:",
				"app.more": "JP_More",
				"app.back": "JP_Back",
				"app.apply": "JP_Apply",
				"title": "JP_More Options",
				"app.close_ticket": "JP_Close Ticket",
				"app.change_priority": "JP_Change Priority",
				"app.change_status": "JP_Change Status",
				"app.esclation": "JP_Esclation",
				"app.custom_field_update": "JP_Custom Field Update",
				"app.pick_ticket": "JP_Pick Ticket",
				"app.ticket_selected": "JP_Ticket Selected",
				"app.close_state": "JP_Close State",
				"app.unassigned_can_be_picked": "JP_Only the tickets that are unassigned can be picked",
				"app.ticket": "JP_Ticket",
				"app.tickets": "JP_Tickets",
				"app.assgined_to_you": "JP_wiil be assigned to you",
				"app.select_state": "JP_Select State",
				"app.selectOption":"Select Option"
			}
			break;
		case 'fr': //French Translations
			messages = {
				"app.change_priority": " Modifier la priorité pour : ",
				"app.high": " Élevée ",
				"app.medium": " Moyenne ",
				"app.low": " Faible ",
				"app.select_State": " Sélectionner le statut ",
				"app.change_Escalation": " Modifier l’escalade à ",
				"app.escalate": " Escalade ",
				"app.de_escalate": " Désescalade ",
				"app.closure_Comment": " Commentaire de clôture ",
				"app.messagejsx_header": " Vous avez sélectionné des billets parmi plusieurs campagnes ",
				"app.messagejsx_para": " Le champ personnalisé peut être mis à jour si vous sélectionnez des billets de la même campagne. ",
				"app.update_ticket_html": " Type de champ sélectionné non pris en charge par cette application ",
				"app.select_Custom_Field": " Sélectionner les personnalisés disponibles ",
				"app.more": " Plus ",
				"app.back": " Retour ",
				"app.apply": " Appliquer ",
				"title": " Plus d’options ",
				"app.close_ticket": " Fermer le ticket ",
				"app.change_priority": " Modifier la priorité ",
				"app.change_status": " Modifier le statut ",
				"app.esclation": " Escalade ",
				"app.custom_field_update": " Mise à jour des champs personnalisés ",
				"app.pick_ticket": " Sélectionner un ticket ",
				"app.ticket_selected": " Ticket sélectionné ",
				"app.close_state": " Fermer le statut ",
				"app.unassigned_can_be_picked": " Seuls les tickets qui ne sont pas affectés peuvent être prélevés ",
				"app.ticket": " Ticket ",
				"app.tickets": " Tickets ",
				"app.assgined_to_you": " vous sera assigné ",
				"app.select_state": "«  Sélectionner le statut ",
				"app.selectOption":"Sélectionnez une option"
			}
			break;
		case 'ar'://Arabic Translations
			messages = {
				"app.change_priority": "غيّر الأولوية إلى:",
				"app.high": "مرتفعة",
				"app.medium": "متوسطة",
				"app.low": "منخفضة",
				"app.select_State": "حدد الحالة",
				"app.change_Escalation": "غيّر التصعيد إلى:",
				"app.escalate": "تصعيد",
				"app.de_escalate": "تهدئة",
				"app.closure_Comment": "إغلاق التعليق",
				"app.messagejsx_header": "لقد حددتَ تذاكر من حملات متعددة",
				"app.messagejsx_para": "يمكن تحديث الحقل المخصص إذا حددتَ تذاكر من الحملة نفسها",
				"app.update_ticket_html": "نوع الحقل المحدد غير مدعوم من قبل هذا التطبيق",
				"app.select_Custom_Field": "حدد حقل مخصص:",
				"app.more": "المزيد",
				"app.back": "رجوع",
				"app.apply": "تطبيق",
				"title": "المزيد من الخيارات",
				"app.close_ticket": "إغلاق تذكرة",
				"app.change_priority": "تغيير الأولوية",
				"app.change_status": "تغيير الحالة",
				"app.esclation": "التصعيد",
				"app.custom_field_update": "تحديث حقل مخصص",
				"app.pick_ticket": "اختيار تذكرة",
				"app.ticket_selected": "التذكرة المختارة",
				"app.close_state": "إغلاق الحالة",
				"app.unassigned_can_be_picked": "يمكن اختيار التذاكر التي لم يتم تخصيصها فقط",
				"app.ticket": "التذكرة",
				"app.tickets": "التذاكر",
				"app.assgined_to_you": "سيتم تعيينها لك",
				"app.select_state": "حدد الحالة",
				"app.selectOption":"حدد الخيار"
			}
			break;
		case 'tr':  //Turkish Translations
			messages = {
				"app.change_priority": "TR_Change_Priority_to:",
				"app.high": "TR_High",
				"app.medium": "TR_Medium",
				"app.low": "TR_Low",
				"app.select_State": "TR_Select_State",
				"app.change_Escalation": "TR_Change_Escalation_to:",
				"app.escalate": "TR_Escalate",
				"app.de_escalate": "TR_De-escalate",
				"app.closure_Comment": "TR_Closure_Comment",
				"app.messagejsx_header": "TR_You have selected tickets from multiple campaigns",
				"app.messagejsx_para": "TR_Custom field can be updated if you select tickets from the same campaign",
				"app.update_ticket_html": "TR_Selected Field Type Not Supported by this App",
				"app.select_Custom_Field": "TR_Select Custom Field:",
				"app.more": "TR_More",
				"app.back": "TR_Back",
				"app.apply": "TR_Apply",
				"title": "TR_More Options",
				"app.close_ticket": "TR_Close Ticket",
				"app.change_priority": "TR_Change Priority",
				"app.change_status": "TR_Change Status",
				"app.esclation": "TR_Esclation",
				"app.custom_field_update": "TR_Custom Field Update",
				"app.pick_ticket": "TR_Pick Ticket",
				"app.ticket_selected": "TR_Ticket Selected",
				"app.close_state": "TR_Close State",
				"app.unassigned_can_be_picked": "TR_Only the tickets that are unassigned can be picked",
				"app.ticket": "TR_Ticket",
				"app.tickets": "TR_Tickets",
				"app.assgined_to_you": "TR_wiil be assigned to you",
				"app.select_state": "TR_Select State",
				"app.selectOption":"TR_Select Option"
			}
			break;
		case 'de': //Dutch Translations
			messages = {
				"app.change_priority": "DE_Change_Priority_to:",
				"app.high": "DE_High",
				"app.medium": "DE_Medium",
				"app.low": "DE_Low",
				"app.select_State": "DE_Select_State",
				"app.change_Escalation": "DE_Change_Escalation_to:",
				"app.escalate": "DE_Escalate",
				"app.de_escalate": "DE_De-escalate",
				"app.closure_Comment": "DE_Closure_Comment",
				"app.messagejsx_header": "DE_You have selected tickets from multiple campaigns",
				"app.messagejsx_para": "DE_Custom field can be updated if you select tickets from the same campaign",
				"app.update_ticket_html": "DE_Selected Field Type Not Supported by this App",
				"app.select_Custom_Field": "DE_Select Custom Field:",
				"app.more": "DE_More",
				"app.back": "DE_Back",
				"app.apply": "DE_Apply",
				"title": "DE_More Options",
				"app.close_ticket": "DE_Close Ticket",
				"app.change_priority": "DE_Change Priority",
				"app.change_status": "DE_Change Status",
				"app.esclation": "DE_Esclation",
				"app.custom_field_update": "DE_Custom Field Update",
				"app.pick_ticket": "DE_Pick Ticket",
				"app.ticket_selected": "DE_Ticket Selected",
				"app.close_state": "DE_Close State",
				"app.unassigned_can_be_picked": "DE_Only the tickets that are unassigned can be picked",
				"app.ticket": "DE_Ticket",
				"app.tickets": "DE_Tickets",
				"app.assgined_to_you": "DE_wiil be assigned to you",
				"app.select_state": "DE_Select State",
				"app.selectOption":"DE_Select Option"
			}
			break;
		case 'th':  //Thai Translations
			messages = {
				"app.change_priority": "เปลี่ยนลำดับความสำคัญเป็น:",
				"app.high": "สูง",
				"app.medium": "กลาง",
				"app.low": "ต่ำ",
				"app.select_State": "เลือกสถานะ",
				"app.change_Escalation": "เปลี่ยนการเลื่อนระดับเป็น:",
				"app.escalate": "เลื่อนระดับ",
				"app.de_escalate": "ลดระดับ",
				"app.closure_Comment": "ปิดความคิดเห็น",
				"app.messagejsx_header": "คุณได้เลือกตั๋วจากหลายแคมเปญ",
				"app.messagejsx_para": "ช่องข้อมูลแบบกำหนดเองสามารถอัปเดตได้หากคุณเลือกตั๋วจากแคมเปญเดียวกัน",
				"app.update_ticket_html": "แอปนี้ไม่รองรับประเภทช่องข้อมูลที่เลือก",
				"app.select_Custom_Field": "เลือกช่องข้อมูลแบบกำหนดเอง:",
				"app.more": "เพิ่มเติม",
				"app.back": "กลับ",
				"app.apply": "ใช้",
				"title": "ตัวเลือกเพิ่มเติม",
				"app.close_ticket": "ปิดตั๋ว",
				"app.change_priority": "เปลี่ยนลำดับความสำคัญ",
				"app.change_status": "เปลี่ยนสถานะ",
				"app.esclation": "การเลื่อนระดับ",
				"app.custom_field_update": "การอัปเดตช่องข้อมูลแบบกำหนดเอง",
				"app.pick_ticket": "เลือกตั๋ว",
				"app.ticket_selected": "เลือกตั๋วแล้ว",
				"app.close_state": "ปิดสถานะ",
				"app.unassigned_can_be_picked": "สามารถเลือกเฉพาะตั๋วที่ยังไม่ได้กำหนด",
				"app.ticket": "ตั๋ว",
				"app.tickets": "ตั๋ว",
				"app.assgined_to_you": "จะกำหนดให้คุณ",
				"app.select_state": "เลือกสถานะ",
				"app.selectOption":"เลือกตัวเลือก"
			}

	}
	return messages;
}


