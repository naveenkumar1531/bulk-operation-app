(function(){
"use strict";
///////
var __client = null;
var __iframeId = getParameterByName("iframeId");
var __appId = getParameterByName("appId");
var __instanceId = getParameterByName("instanceId");
///////
var __requestIdVsCallback = {};
var __requestIdVstimeout = {};
///////
var __requestTimeout = 10000; // 5 second

function getParameterByName(name) {
	var url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex
			.exec(url);
	if (!results) {
		return null;
	}
	if (!results[2]) {
		return '';
	}
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

window.AmeyoClient = {};
AmeyoClient.init = function() {
	if(!__client){
		__client = new Client();
	}
	return __client;
};
var getUniqueID = function () {
	 return Math.random().toString(10).substr(2, 12);
	 };
function doPostMessage(message) {
	window.parent.postMessage(message, "*");
}

 function processPostMessage(event) {
	  var responseObject = event.data;
	  try {
	    responseObject = JSON.parse(responseObject);
	  	handleApplicationResponse(responseObject);
	  } catch (e) {
	  	console.log("ameyo_aap_sdk : invalid json response returned from ameyo application " + e);
	  }
}

function handleApplicationResponse(responseObject){
	var service = responseObject.service;
	var callbackObject = __requestIdVsCallback[responseObject.requestId];
	if(!callbackObject){
		console.log("ameyo_aap_sdk : ameyo application response received after request timeout. service : " + responseObject.service + ", " + "method : " + responseObject.method);
		return;
	}
	var callback = __requestIdVsCallback[responseObject.requestId][responseObject.status];		
	if(callback){
		callback(responseObject.data);
	}
	clearTimeout(__requestIdVstimeout[responseObject.requestId]);
	delete __requestIdVstimeout[responseObject.requestId]; 
	delete __requestIdVsCallback[responseObject.requestId];
}

function sendTimeoutResponse(requestId){
	var callback = __requestIdVsCallback[requestId]["failure"];		
	if(callback){
		callback({code:300, reason:"request timeout"});
	}
	delete __requestIdVstimeout[requestId]; 
	delete __requestIdVsCallback[requestId];
}
var prepareRequest =function(requestObject){
	requestObject.appId = __appId;
	requestObject.instanceId = __instanceId;
	requestObject.iframeId = __iframeId;
	return requestObject;
}
var Client = function(data) {	
	if (window.attachEvent) {
		window.attachEvent('onmessage', processPostMessage);
	} else {
		window.addEventListener('message', processPostMessage, false);
	}

	this.globaldata = {
		get:function(dataObject,successCallback,failureCallback){
			var uniqueId = getUniqueID();
			var request = prepareRequest({"requestId":uniqueId,"service":"globaldata","method":"get","dataObject":dataObject});
			var message = JSON.stringify(request);
			__requestIdVsCallback[uniqueId] = {success:successCallback,failure:failureCallback};
			__requestIdVstimeout[uniqueId] = setTimeout(sendTimeoutResponse, __requestTimeout, uniqueId);
			doPostMessage(message);
		}
	}
	this.data = {
		get:function(dataObject, successCallback, failureCallback){
			var uniqueId = getUniqueID();
			var request = prepareRequest({"requestId":uniqueId ,"service":"data","method":"get", "dataObject":dataObject});
			var message = JSON.stringify(request);
			__requestIdVsCallback[uniqueId] = {success:successCallback,failure:failureCallback};
			__requestIdVstimeout[uniqueId] = setTimeout(sendTimeoutResponse, __requestTimeout, uniqueId);
			doPostMessage(message);
		},
		getList:function(dataObject, successCallback, failureCallback){
			var uniqueId = getUniqueID();
			var request = prepareRequest({"requestId":uniqueId ,"service":"data","method":"getList", "dataObject":dataObject});
			var message = JSON.stringify(request);
			__requestIdVsCallback[uniqueId] = {success:successCallback,failure:failureCallback};
			__requestIdVstimeout[uniqueId] = setTimeout(sendTimeoutResponse, __requestTimeout, uniqueId);
			doPostMessage(message);
		},
		update:function(dataObject, updateData, successCallback, failureCallback){
			var uniqueId = getUniqueID();
			var request = prepareRequest({"requestId":uniqueId,"service":"data","method":"update","dataObject":dataObject,"updateData":updateData});
			var message = JSON.stringify(request);
			__requestIdVsCallback[uniqueId] = {success:successCallback,failure:failureCallback};
			__requestIdVstimeout[uniqueId] = setTimeout(sendTimeoutResponse, __requestTimeout, uniqueId);
			doPostMessage(message);
		},
		action:function(dataObject, actionType, actionData, successCallback, failureCallback){
			var uniqueId = getUniqueID();
			var request = prepareRequest({"requestId":uniqueId,"service":"data","method":"action","dataObject":dataObject,"actionType":actionType,"actionData":actionData});
			var message = JSON.stringify(request);
			__requestIdVsCallback[uniqueId] = {success:successCallback,failure:failureCallback};
			__requestIdVstimeout[uniqueId] = setTimeout(sendTimeoutResponse, __requestTimeout, uniqueId);
			doPostMessage(message);
		},

	}
	this.ui = {
		trigger:function(operation, data, successCallback, failureCallback){
			var uniqueId = getUniqueID();
			var request = prepareRequest({"requestId":uniqueId,"service":"ui",method:"trigger","operation":operation,"operationData":data});
			var message = JSON.stringify(request);
			__requestIdVsCallback[uniqueId] = {success:successCallback,failure:failureCallback};
			__requestIdVstimeout[uniqueId] = setTimeout(sendTimeoutResponse, __requestTimeout, uniqueId);
			doPostMessage(message);
		}
	}
};
})();



