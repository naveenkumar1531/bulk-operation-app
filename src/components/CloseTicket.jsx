import React, { Component } from 'react'
import { renderTextField, renderSelect, renderTextArea } from './form-elements/FormElements'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import FooterPanel from './Footer'
import { closeTicket, getExternalState, getTicketState } from '../middlewares/bulkActionMiddleware'
import { FormattedMessage } from 'react-intl';


class CloseTicket extends Component {

  constructor(props) {
    console.log("constructor");
    super(props)
  }
  componentWillMount() {
    console.log("inside componentWillMount");
    const { onGetExternalState } = this.props
    onGetExternalState;

  }


  render() {

    return <CloseTicketForm setButtonStatusData={this.props.setButtonStatusData} onSubmit={this.props.handleSubmit} externalStateList={this.props.externalStateList} comment={this.props.comment}
      values1={this.props.values1} />
  }
}

const validate = (values) => {
  console.log("inside validator");
  const errors = {}
  if (!values.comment) {
    errors.comment = ''
  }

  return errors
}
let CloseTicketForm = props => {
  const { handleSubmit, externalStateList, comment, setButtonStatusData, values1 } = props;
  var shouldEnableApply = false;
  if (values1.ticket_status !== undefined && values1.ticket_status !== '' && values1.comment !== undefined &&
    values1.comment !== '') {
    shouldEnableApply = true
  }
  var options = [];
  options.push(<option value="" key="11111">Select State</option>)
  if (externalStateList.externalStates) {
    var list = externalStateList.externalStates;
    var closeState = list.CLOSED;
    for (let j = 0; j < closeState.length; j++) {
      options.push(<option value={closeState[j]} key={j}>{closeState[j]}</option>)
    }
  }
  return (
    <form onSubmit={handleSubmit}>
      <div className="bulk-action">
        <div className="bulk-action__content">
          <div id="ticketClose" className="row" >
            <div className="col s12 no-padding">
              <div className="row select-no-padding">
                <Field name="ticket_status" component={renderSelect}
                  label={<FormattedMessage
                    id='app.close_state'
                    defaultMessage={"Close State"}
                  />}
                  s={6} m={6}>
                  {options}
                </Field>
              </div>
              <div className="row">
                <div className="col s12 m12 no-padding">
                  <div className="input-field">
                    <Field name="comment" id="comment" className="materialize-textarea" component={renderTextArea} />
                    <label htmlFor="comment" className="active"><FormattedMessage
                      id='app.closure_Comment'
                      defaultMessage={"Closure Comment"}
                    /><span className="required"> *</span></label>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <FooterPanel enabledApplyButton={shouldEnableApply} source={"CloseTicket"} />
      </div>
    </form>
  )
}

CloseTicketForm = reduxForm({
  form: 'form1',
  validate
})(CloseTicketForm)
const selector = formValueSelector('form1')
CloseTicket = connect(
  state => ({
    values1: state.form.form1 !== undefined ? (state.form.form1.values !== undefined ?
      state.form.form1.values : false) : false

  }))(CloseTicket)
const mapStateToProps = (state, ownProps) => {
  return {
    externalStateList: getTicketState(state),
    comment: selector(state, 'comment')
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  var updateState = (values) => {
    dispatch(closeTicket(values));
  }
  
  return {
    onGetExternalState: dispatch(getExternalState()),
    handleSubmit: updateState

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(CloseTicket)





