import { push } from 'react-router-redux';
import {  hashHistory, browserHistory} from 'react-router';

function redirectToMain(){
 
     hashHistory.push('/main');
  
}




var successTicketArray =[];
var failureTicketArray =[];
export function invokeAsBatch(action,batchSize,dataArray=[],actionData) {
    successTicketArray=[];
    failureTicketArray=[];
	return (dispatch) => {
	
	var totalArray = dataArray.length;
	var chunkArray = splitArrayIntoChunk(batchSize,dataArray);
	var Index = 0;
	if(chunkArray.length > 0) {
		dispatch(updateData(action,batchSize,chunkArray,totalArray,actionData,Index));
	}
  }
}

export function updateData(action,batchSize,chunkArray,totalArray,actionData,Index) {
   return (dispatch) => {	
	var client = window.AmeyoClient.init();
		 client.contextData.bulkAction("ticket",action,chunkArray[Index], actionData)
		 .then(
		 	function(successData) {
		 		successTicketArray.push(successData);
				if(Index < chunkArray.length-1) {
					Index = Index +1;
	   			   dispatch(updateData(action,batchSize,chunkArray,totalArray,actionData,Index))		
	            } else {
	            	sendSuccessNotification(successTicketArray);
	            	if(failureTicketArray.length > 0) {
	            		sendFailureNotification(totalArray,failureTicketArray)
	            	}
	            	redirectToMain();
	            }

		 	}
		  )
		 .catch(
		 	function(failureData) {
		 		failureTicketArray.push(failureData);
		 		//console.log('failureData111',failureData)
		 		if(Index < chunkArray.length-1) {
					Index = Index +1;
	   			    dispatch(updateData(action,batchSize,chunkArray,actionData,Index))		
	            }else {
	            	if(successTicketArray.length > 0) {
	            		sendSuccessNotification(successTicketArray.length);
	            	}
	            	sendFailureNotification(totalArray,failureTicketArray)
	            	redirectToMain();
	            }
		    }
		);
	}
	
}

function splitArrayIntoChunk(batchSize,dataArray){
	var row = []
	if(batchSize >= dataArray.length) {
		 row.push(dataArray)
	} else {
		
		var arrayLen = dataArray.length;
		for(let i =0; i <arrayLen;i+=batchSize){
		 row.push(dataArray.slice(i,i+batchSize))
		}
		
	}
	return row
}



// function extractTicketId(ticketObject) {
//   var array=[]
//   Object.keys(ticketObject).forEach((key)=> {
//   	array.push(ticketObject[key].ticketId)
//   })

//   return array;

// }

function sendSuccessNotification(ticketArray){
   var ticketCount =0;
   var arrLen =  ticketArray.length;
   for(let i=0; i<arrLen;i++) {
   		ticketCount += Object.keys(ticketArray[i]).length;
   }
   var client = window.AmeyoClient.init();
   var m2     = ticketCount>1?' tickets':' ticket';
   var message =  ticketCount + m2 +" updated successfully";
      var toastNotificationData = {
          type:"success",
          content:message
      }
      client.interface.trigger("toastNotification", toastNotificationData).
      then(successCallback).catch(failureCallback);
}
function displayToastNotification(message){
  var client = window.AmeyoClient.init();
  var toastNotificationData = {
    type    : "error",
    content : message
  }
  client.interface.trigger("toastNotification" ,toastNotificationData).then(successCallback).catch(failureCallback);
}

function sendFailureNotification(ticketCount,failureData){
  
/* Sample error messages 
Toast 1 : Unable to process {13} tickets
Toast 2 : {6} {out of {13} Ticket not processed due to} {already asssigned}
Toast 3 : {5} {out of {13} Ticket not processed due to} {mandatory field not provided or resource not allocated}
Toast 4 : {2} {out of {13} Ticket not processed due to} {internal failure} */

var m2     = ticketCount>1?' tickets':' ticket';
var message = '';
var msgCount ='';
var msg='';
var errorMessageGroup = window.errorMessageGroup();
var errorMessagesList = window.errorMessagesList();
var failureResponse={};
var count=0;
var tmpErrorCodeCurrent;
var tmpErrorCodePrevious;
var isErrorCodeGrpFlag=0;
var msgValue=0;
var grpMsg='';
var grpCount=0;

for ( var key in failureData ){
  tmpErrorCodeCurrent = failureData[key].code;
  count = (tmpErrorCodeCurrent==tmpErrorCodePrevious)?count:0;
  failureResponse[failureData[key].code]= ++count;
  tmpErrorCodePrevious = failureData[key].code;

}
var errorCodes = Object.keys(failureResponse);
for (var k in errorCodes){
  if ((errorMessageGroup.indexOf(errorCodes[k]))<0){
    msg = errorMessagesList[errorCodes[k]];
    msgCount= failureResponse[errorCodes[k]];
    if (errorCodes.length==1){
      message = 'Unable to process '+msgCount+''+m2+' as '+msg;  
      displayToastNotification(message);
    } else{
      message = msgCount+' Out of '+ticketCount+''+m2+' not processed due to'+msg;  
      displayToastNotification(message);
    }
    
  }else {
    grpMsg   = errorMessagesList[errorCodes[k]];
    grpCount = failureResponse[errorCodes[k]];
    msgValue = msgCount + msgValue;
    isErrorCodeGrpFlag  = 1;   
  } 
}
if (isErrorCodeGrpFlag  == 1){
  if (isErrorCodeGrpFlag==0){
    message = msgValue+'Out of '+ticketCount+''+m2+' not processed due to '+grpMsg;  
  }else {
    message = 'Unable to process '+grpCount+''+m2+' as '+grpMsg;  
  }
  displayToastNotification(message);
}
}


function successCallback() {

}
function failureCallback() {
	
}