
export const  setExternalState=(externalState)=>{
        return{
                type:'SET_EXTERNAL_STATE',
                externalState
        }
}
export const  setTicketList=(ticketList)=>{
        return{
                type:'SET_TICKET_LIST',
                ticketList
        }
}

export const  setLoaderStatus=(status)=>{
        return{
                type:'SET_LOADER',
                status
        }
}
export const setButtonType = (btnType)=>{
        
        return {
                type:'SET_BUTTON_TYPE',
                btnType       
        }
}

export const setCustomFieldsDetails = (fieldsDetails) =>{
        return {
                type: 'SET_CUSTOM_FIELDS_DETAILS',
                fieldsDetails
        }
}


