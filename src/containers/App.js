import React, { Component } from 'react'
import {IntlProvider,addLocaleData} from 'react-intl';
import locale_en from 'react-intl/locale-data/en';
import locale_ja from 'react-intl/locale-data/ja';
import locale_fr from 'react-intl/locale-data/fr';
import locale_ar from 'react-intl/locale-data/ar';
import locale_de from 'react-intl/locale-data/de';
import locale_tr from 'react-intl/locale-data/tr';
import locale_th from 'react-intl/locale-data/th';


addLocaleData([...locale_en,...locale_ja,...locale_fr,...locale_ar,...locale_de,...locale_tr,...locale_th]);

class App extends Component {
  constructor(props) {
    super(props)
  }
 render() {
        return (
        <div>
        <IntlProvider locale={this.props.route.defaultLocale} messages={this.props.route.messages}>
            {this.props.children}
      </IntlProvider>
        </div>
    )
  }
}
export default App

