import { reducer as formReducer } from 'redux-form'
import uiSelection from './UISelectionReducer'
import getTicketList from './SelectedTicketList'
import {routerReducer } from 'react-router-redux'
import {combineReducers} from 'redux'

export const rootReducer = combineReducers({
 form: formReducer,     // <---- Mounted at 'form'
 routing: routerReducer,
 uiSelection,
 getTicketList
 

 });
export default rootReducer


