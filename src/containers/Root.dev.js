import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import { Router, Route,IndexRoute } from 'react-router'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
const lightMuiTheme = getMuiTheme(lightBaseTheme);


import App from './App';
import MainPage from '../components/MainPage';
import ModalButton from '../components/ModalButton';
import CloseTicket from '../components/CloseTicket';
import ChangePriority from '../components/ChangePriority';
import ChangeStatus from '../components/ChangeStatus';
import EscalateTicket from '../components/EscalateTicket';
import UpdateTicket from '../components/UpdateTicket';
import PickTicket from '../components/PickTicket';
const muiTheme = getMuiTheme({
  appBar: {
    height: 5,
    width: 5,
  },
});
const defaultRoute =  window.getDefaultRoute();
const Root = ({ store, history,defaultLocale,messages }) => (
<Provider store={store}>
 <MuiThemeProvider muiTheme={muiTheme}>
  <Router  history={history}>
    <Route path={defaultRoute} component={App} defaultLocale={defaultLocale} messages={messages}>
    <IndexRoute component={ModalButton} />
    <Route path="/openModal" component={ModalButton} />
    <Route path="/main" component={MainPage} />
    <Route path="/close-ticket" component={CloseTicket} />
     <Route path="/change-priority" component={ChangePriority} />
     <Route path="/change-status" component={ChangeStatus} />
     <Route path="/escalate" component={EscalateTicket} />
     <Route path="/update-ticket" component={UpdateTicket} />
      <Route path="/pick-ticket" component={PickTicket} />
     </Route>
   </Router>
   </MuiThemeProvider>
 </Provider>
 )
Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
}
export default Root



