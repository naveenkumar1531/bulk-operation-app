import React, { Component } from 'react'
import {renderRadioButton,renderAutoSuggest} from './form-elements/FormElements'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import FooterPanel from './Footer'
import {changeTicketPriority} from '../middlewares/bulkActionMiddleware'
import {FormattedMessage} from 'react-intl';



class ChangePriority1 extends Component {

  constructor(props) {
    super(props) 
  }
 
 render() {
      
          return <ChangePriorityForm onSubmit={this.props.handleSubmit} />
  }
}
let ChangePriorityForm=props => {
  const{handleSubmit}=props;
  
  return(
  <form onSubmit={handleSubmit}>
  <div className="bulk-action">
  <div className="bulk-action__content">
    <div id="ticketPriority" className="row">
 <div className="col s12 m12 no-padding">
            <div className="row">
                                        <div className="col s12 m12 no-padding">
                                                <h4><FormattedMessage
                  id='app.change_priority'
                  defaultMessage={"Change Priority to:"}
                /></h4>
                                        </div>
                                </div>
                                <div className="row">
                                        <div className="col s12 no-padding">
                                          
                                            <Field name="auto" id="high" component={renderAutoSuggest} type="text" value="Apple"/>
                                                
                                           
                                        </div>
                                        
                                </div>

                                hghjgjj
                        </div>
                </div>
    
  </div>
  <FooterPanel/>
  </div>

</form>
   )
}

ChangePriorityForm= reduxForm({
  form: 'priorityform11',              // <------ same form name
   initialValues: {
      auto: 'Apple'
    }
  
  
  })(ChangePriorityForm)

const mapStateToProps = (state, ownProps) => {
  return {
     
   }
}
const mapDispatchToProps = (dispatch, ownProps) => {
   var updatePriority=(values)=>{
        dispatch(changeTicketPriority(values));
  }
 
  return {

    handleSubmit:updatePriority
    
  }
}
 export default connect(mapStateToProps,mapDispatchToProps)(ChangePriority1)

 



