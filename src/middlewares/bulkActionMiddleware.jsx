import {extractTicketId,getBatchSize,invokeAsBatch} from './CustomUtils'
import {
    setExternalState,
    setTicketList,
    setLoaderStatus,
    setCustomFieldsDetails,
    setButtonType
} from '../actions/bulkAction'
import {
    push
} from 'react-router-redux';
import {
    hashHistory,
    browserHistory
} from 'react-router';

var ticketNotAssignedGroup = [];
var resourceNotAvailableGroup = []
var uiValidationGroup = []
var otherErrorGroup = []
var alreadyAssignedGroup = []


function redirectToMain() {

    hashHistory.push('/main');

}
export function closeTicket(values) {

    return (dispatch, getState) => {
        var state = getState();
        var client = window.AmeyoClient.init();
        var ticketList = (state.getTicketList.selectedTickets !== undefined ? state.getTicketList.selectedTickets : '');
        if (ticketList !== '') {
            // dispatch(setLoaderStatus(true));

            var updateData = {
                externalState: values.ticket_status,
                closureReason: values.comment
            }
            var type = 'update';
            var dataArray = extractTicketId(ticketList)
            dispatch(setLoaderStatus(true))
            invokeAsBatch('bulkUpdate', getBatchSize(), dataArray, updateData, type, sendSuccessNotification, sendFailureMessage)
        }
    }
}

export function changeStatus(values) {
    return (dispatch, getState) => {

        var state = getState();
        var client = window.AmeyoClient.init()
        var ticketList = (state.getTicketList.selectedTickets !== undefined ? state.getTicketList.selectedTickets : '');
        if (ticketList !== '') {
            var updateData = {
                externalState: values.ticket_status
            }
            var type = 'update';
            var dataArray = extractTicketId(ticketList)
            dispatch(setLoaderStatus(true))
            invokeAsBatch('bulkUpdate', getBatchSize(), dataArray, updateData, type, sendSuccessNotification, sendFailureMessage)
        }
    }
}

export function pickTicket(values) {
    return (dispatch, getState) => {
        var state = getState();
        var ticketList = (state.getTicketList.selectedTickets !== undefined ? state.getTicketList.selectedTickets : '');
        if (ticketList !== '') {
            var actionData = {
                "note": "bulk pick with app"
            }
            var type = 'action';
            var dataArray = extractTicketId(ticketList)
            dispatch(setLoaderStatus(true))
            invokeAsBatch('pick', getBatchSize(), dataArray, actionData, type, sendSuccessNotification, sendFailureMessage)
            // .then(sendSuccessNotification)
            // .catch(function(failureMessage) {
            //     console.log('failureData', failureMessage)                
            // })
        }
    }
}

export function changeTicketPriority(values) {
    return (dispatch, getState) => {
        var state = getState();
        var ticketList = (state.getTicketList.selectedTickets !== undefined ? state.getTicketList.selectedTickets : '');
        if (ticketList !== '') {
            //      dispatch(setLoaderStatus(true));
            var updateData = {
                priority: values.ticket_priority
            }
            var type = 'update';
            var dataArray = extractTicketId(ticketList)
            dispatch(setLoaderStatus(true))
            invokeAsBatch('bulkUpdate', getBatchSize(), dataArray, updateData, type, sendSuccessNotification, sendFailureMessage)

        }
    }
}

export function escalateTicket(values) {
    return (dispatch, getState) => {
        var state = getState();
        var ticketList = (state.getTicketList.selectedTickets !== undefined ? state.getTicketList.selectedTickets : '');
        if (ticketList !== '') {
            var updateData = {}
            if (values.ticket_escalate == "true") {
                updateData["escalate"] = true
            } else {
                updateData["escalate"] = false
            }
            var type = 'update';
            var dataArray = extractTicketId(ticketList)
            dispatch(setLoaderStatus(true))
            invokeAsBatch('bulkUpdate', getBatchSize(), dataArray, updateData, type, sendSuccessNotification, sendFailureMessage)
        }
    }
}
export function setCustomFields(values) {
    return (dispatch, getState) => {
        var state = getState();

        var client = window.AmeyoClient.init();
        client.globalData.get("customFields")
            .then((successCallback) => {
                dispatch(setCustomFieldsDetails(successCallback));
            })
            .catch((failureCallback) => {
                console.log("failure response=" + JSON.stringify(failureCallback))
            })
    }
}


export function updateCustomFields(values) {
    return (dispatch, getState) => {
        var state = getState();
        var ticketList = (state.getTicketList.selectedTickets !== undefined ? state.getTicketList.selectedTickets : '');
        var form = (state.form !== undefined ? state.form.customFieldform : '');
        var formCustomField = (form.values !== undefined ? form.values.customFields : '');
        if (formCustomField != '') {
            var stateCustomFields = (state.getTicketList.customFields !== undefined ? state.getTicketList.customFields : '');
            var selectedCustomFieldDataTypeVar = selectedCustomFieldDataType(stateCustomFields, formCustomField);
            var selectedCustomTypeVar = selectedCustomType(stateCustomFields, formCustomField);

            var customFields = {};
            var tempVar = [];
            if (selectedCustomTypeVar == 'CheckBox') {
                var formCustomFieldValue = (form.values !== undefined ? form['values'] : undefined);
                if (formCustomFieldValue != undefined) {
                    for (var valuesKey in formCustomFieldValue) {
                        if (selectedCustomFieldDataTypeVar == 'Integer') {
                            if (valuesKey != 'customFields') {
                                tempVar.push(parseInt(valuesKey));
                            }
                        } else if (selectedCustomFieldDataTypeVar == 'Decimal') {
                            if (valuesKey != 'customFields') {
                                tempVar.push(parseFloat(valuesKey));
                            }
                        } else if (selectedCustomFieldDataTypeVar == 'Boolean') {
                            if (valuesKey != 'customFields') {
                                tempVar.push(JSON.parse(valuesKey));
                            }
                        } else {
                            if (valuesKey != 'customFields') {
                                tempVar.push(valuesKey);
                            }
                        }
                    }
                    formCustomFieldValue = tempVar;
                }
            } else {
                var formCustomFieldValue = (form.values !== undefined ? form['values'][formCustomField] : undefined);
                if (typeof formCustomFieldValue === 'object') {
                    if (selectedCustomFieldDataTypeVar == 'Integer') {
                        for (var index in formCustomFieldValue) {
                            tempVar.push(parseInt(formCustomFieldValue[index]));
                        }
                    } else if (selectedCustomFieldDataTypeVar == 'Decimal') {
                        for (var index in formCustomFieldValue) {
                            tempVar.push(parseFloat(formCustomFieldValue[index]));
                        }
                    } else {
                        for (var index in formCustomFieldValue) {
                            tempVar.push(formCustomFieldValue[index]);
                        }
                    }
                    formCustomFieldValue = tempVar;

                } else {
                    formCustomFieldValue = (selectedCustomFieldDataTypeVar == 'Integer' ? parseInt(formCustomFieldValue) : (selectedCustomFieldDataTypeVar == 'Decimal' ? parseFloat(formCustomFieldValue) : formCustomFieldValue));
                }
            }
            customFields[formCustomField] = formCustomFieldValue;
        }

        if (ticketList !== '') {
            dispatch(setLoaderStatus(true));
            var updateData = {
                customFields: customFields
            }
            var type = 'update';
            var dataArray = extractTicketId(ticketList)
            dispatch(setLoaderStatus(true))
            invokeAsBatch('bulkUpdate', getBatchSize(), dataArray, updateData, type, sendSuccessNotification, sendFailureMessage)
        }
    }
}
export function getCustomFieldsDetails(state) {
    // console.log("fetching state",state);
    var campaignCustomFields = {};
    var allCustomFields = state.getTicketList === undefined ? undefined : state.getTicketList.customFields;
    var selectedTicketsCampaign = state.getTicketList === undefined ? '' : state.getTicketList.selectedTicketsCampaign
    //console.log("selectedTicketsCampaign",selectedTicketsCampaign[0]);
    if (allCustomFields != undefined) {
        campaignCustomFields = allCustomFields[selectedTicketsCampaign[0]];

    }
    return campaignCustomFields;
}

function selectedCustomFieldDataType(customFieldsDetails, customFieldsValue) {
    for (var key in customFieldsDetails) {
        for (var innerkey in customFieldsDetails[key]) {
            if (customFieldsDetails[key][innerkey].customFieldId == customFieldsValue) {
                return customFieldsDetails[key][innerkey].customFieldDataType;
            }

        }
    }
}

function selectedCustomType(customFieldsDetails, customFieldsValue) {
    for (var key in customFieldsDetails) {
        for (var innerkey in customFieldsDetails[key]) {
            if (customFieldsDetails[key][innerkey].customFieldId == customFieldsValue) {
                return customFieldsDetails[key][innerkey].customFieldType;
            }
        }
    }
}

function displayToastNotification(type, message) {
    var client = window.AmeyoClient.init();
    var toastNotificationData = {
        type: type,
        content: message
    }
    client.interface.trigger("toastNotification", toastNotificationData).then(successCallback).catch(failureCallback);
}

function groupErrorMessage(failureData) {
    ticketNotAssignedGroup = [];
    resourceNotAvailableGroup = [];
    uiValidationGroup = [];
    alreadyAssignedGroup = []
    otherErrorGroup = [];
    var length = failureData.length
    for (let i = 0; i < length; i++) {
        let data = failureData[i]
        let message = data.message
        if (message.match(/not assigned/g) !== null) {
            ticketNotAssignedGroup.push(message);
        } else if (message.match(/resource not allocated/g) !== null) {
            resourceNotAvailableGroup.push(message)
        } else if (message.match(/mandatory/g) !== null) {
            uiValidationGroup.push(message)
        } else if (message.match(/interaction.allready.assigned/g) !== null) {
            alreadyAssignedGroup.push(message)
        } else {
            otherErrorGroup.push(message)
        }

    }
    return length
}

export function sendSuccessNotification(totalTicket, ticketArray, dispatch) {

    try {
        //console.log('ticketArray',JSON.stringify(ticketArray))

        var failureData = []
        var ticketCount = 0;
        var arrLen = ticketArray.length;
        for (let i = 0; i < arrLen; i++) {
            var data = ticketArray[i];
            Object.keys(data).forEach((key) => {
                if (data[key].status === 'SUCCESS') {
                    ticketCount += 1;
                } else {
                    failureData.push(data[key])
                }
            })
        }
        var failureCount = failureData.length
        console.log('failureCount>>>', failureCount)
        console.log('ticketCount>>>', ticketCount)
        var client = window.AmeyoClient.init();
        if (ticketCount > 0 && failureCount == 0) {
            var m2 = ticketCount > 1 ? ' tickets' : ' ticket';
            var message = ticketCount + m2 + " updated successfully";
            var toastNotificationData = {
                type: "success",
                content: message
            }
            client.interface.trigger("toastNotification", toastNotificationData).
                then(successCallback).catch(failureCallback);
        } else {
            sendFailureMessage(totalTicket, failureData, ticketCount)
        }

        redirectToMain()
    } catch (e) {

    }



    //dispatch(setLoaderStatus(false))

}

function sendFailureMessage(ticketCount, failureData, successCount) {

    var message = '';
    var m2 = ticketCount > 1 ? ' tickets' : ' ticket';
    var length = failureData.length
    if (length > 0) {
        console.log("ticketCount----", ticketCount)
        console.log("m2------", m2)
        //message = 'Unable to process ' + length + m2;
        //displayToastNotification(message);
        //console.log("failureData>>>>",failureData)
        groupErrorMessage(failureData);
    }

    if (ticketNotAssignedGroup.length > 0) {
        //message = ticketNotAssignedGroup.length + ' Out of ' + length + ' not processed due to ticket not assigned to user';
        message = message + ticketNotAssignedGroup.length + (ticketNotAssignedGroup.length > 1 ? ' tickets' : ' ticket') + ' could not be updated because it was not assigned to you. ';
        //displayToastNotification(message);
    }
    if (resourceNotAvailableGroup.length > 0) {
        message = message + resourceNotAvailableGroup.length + (resourceNotAvailableGroup.length > 1 ? ' tickets' : ' ticket') + ' not processed due to resource not available. ';
        //displayToastNotification(message);
    }
    if (uiValidationGroup.length > 0) {
        message = message + uiValidationGroup.length + (uiValidationGroup.length > 1 ? ' tickets' : ' ticket') + ' not processed due to mandatory fields. ';
        //displayToastNotification(message);
    }
    if (alreadyAssignedGroup.length > 0) {
        message = message + alreadyAssignedGroup.length + (alreadyAssignedGroup.length > 1 ? ' tickets' : ' ticket') + ' could not be updated because it was already assigned. ';
        //displayToastNotification(message);
    }
    if (otherErrorGroup.length > 0) {
        message = message + otherErrorGroup.length + (otherErrorGroup.length > 1 ? ' tickets' : ' ticket') + ' not processed due to internal error. ';
        //displayToastNotification(message);
    }
    //console.log("successCount>>>",successCount)
    if (successCount > 0 && length > 0) {
        var message = successCount + (successCount > 1 ? ' tickets' : ' ticket') + " were successfully updated. " + message;
        displayToastNotification("alert", message);
    } else if (successCount == 0 && length > 0) {
        displayToastNotification("error", message);

    }

}

export function getExternalState() {
    return (dispatch) => {
        var client = window.AmeyoClient.init();
        client.globalData.get("ticketStates").then((success) => {
            dispatch(setExternalState(success))
        }).catch((failure) => {
            var toastNotificationData = {
                type: "failure",
                content: "Not able to get external state"
            }
            client.interface.trigger("toastNotification", toastNotificationData).then(successCallback).catch(failureCallback);
            //client.ui.trigger("notification", { type: "error", content: "Failed to fetch External States"});
        });

    }

}

function successCallback() {
    //console.log('success');
}

function failureCallback(failure) {
    //console.log('failure'+JSON.stringify(failure));
}
export const getTicketState = (state) => {
    var externalStates = (state.uiSelection !== undefined ? (state.uiSelection.externalStates !== undefined ? (state.uiSelection.externalStates) : undefined) : undefined)

    return {
        externalStates
    }
}
export const getTicketCount = (state) => {

    var ticketCount = (state.getTicketList !== undefined ? (state.getTicketList.selectedTickets !== undefined ? (state.getTicketList.selectedTickets) : undefined) : undefined)
    if (ticketCount !== undefined) {
        ticketCount = ticketCount.length;
    }
    return {
        ticketCount
    }
}

export function getSelectedTicket() {
    return (dispatch, getState) => {
        // console.log('i m here')
        dispatch(setLoaderStatus(false))
        var state = getState();
        var isTicketSelected = (state.getTicketList !== undefined ? (state.getTicketList.selectedTickets !== undefined ? (state.getTicketList.selectedTickets) : undefined) : undefined);
        if (isTicketSelected === undefined) {
            var client = window.AmeyoClient.init();
            client.contextData.get("selectedTickets").then((ticketList) => {
                dispatch(setTicketList(ticketList))
            }).catch((failure) => {
                console.log('Failed to fetch Selected Tickets', failure)
            });

        }
    }
}