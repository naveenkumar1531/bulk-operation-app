const getTicketList=(state={},action) =>{
switch(action.type){
	case 'SET_TICKET_LIST':
	
	//var copy = Object.assign({},state);
	//if(copy.selectedTickets ===undefined){
		var selectedTickets=action.ticketList;
		//console.log('ticketList',selectedTickets)
		var campaignArray= [];
		Object.keys(selectedTickets).forEach((index,key)=>{
			campaignArray.push(selectedTickets[key].campaignId);
		});
		const uniqueCampaign= Array.from(new Set(campaignArray));
		return Object.assign({}, state, { selectedTickets : selectedTickets,selectedTicketsCampaign:uniqueCampaign})
    	// return {
     //            ...state,
     //            selectedTickets:selectedTickets,
     //            selectedTicketsCampaign:uniqueArray
     //    }
	case 'SET_CUSTOM_FIELDS_DETAILS':
		return{
			...state,
			customFields: action.fieldsDetails
		}

	
	default:
		return state;
  }
}
export default getTicketList