import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import {getSelectedTicket} from '../middlewares/bulkActionMiddleware'
import {FormattedMessage} from 'react-intl';
class MainPage extends Component {
  render() {
    var count= this.props.selectedTickets==undefined ? 0 :(this.props.selectedTickets.length);    
      return (
          <div>
          <div className="selected-ticket-count"> <span><FormattedMessage
                  id='app.ticket_selected'
                  defaultMessage={"Ticket Selected"}
                />: {count} </span></div>
    <ul className="actions-listing">
      <li>
        <Link to="/close-ticket"><button type="button" id="ticketCloserBtn" className="btn"  >
        <span><FormattedMessage
                  id='app.close_ticket'
                  defaultMessage={"Close Ticket"}
                /></span><i className="mdi mdi-chevron-right"></i></button></Link>
      </li>
      <li>
        <Link to="/change-priority"><button type="button" id="ticketPriorityBtn" className="btn">
        <span><FormattedMessage
                  id='app.change_priority'
                  defaultMessage={"Change Priority"}
                /></span><i className="mdi mdi-chevron-right"></i></button></Link>
      </li>
      <li>
        <Link to="/change-status"><button type="button" id="ticketStateBtn" className="btn">
        <span><FormattedMessage
                  id='app.change_status'
                  defaultMessage={"Change Status"}
                />
          </span><i className="mdi mdi-chevron-right"></i></button></Link>
      </li>
      <li>
        <Link to="/escalate"><button type="button" id="ticketEscalationBtn" className="btn">
        <span><FormattedMessage
                  id='app.esclation'
                  defaultMessage={"Esclation"}
                /></span><i className="mdi mdi-chevron-right"></i></button></Link>
      </li>
      <li>
        <Link to="/update-ticket"><button type="button" id="ticketUpdateBtn" className="btn" >
        <span><FormattedMessage
                  id='app.custom_field_update'
                  defaultMessage={"Custom Field Update"}
                />
          </span><i className="mdi mdi-chevron-right"></i></button></Link>
      </li>
      <li>
        <Link to="/pick-ticket"><button type="button" id="ticketPickBtn" className="btn" >
        <span><FormattedMessage
                  id='app.pick_ticket'
                  defaultMessage={"Pick Ticket"}
                /></span><i className="mdi mdi-chevron-right"></i></button></Link>
      </li>

    
    </ul> 
    </div>
           
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
     selectedTickets : state.getTicketList === undefined ? '' : state.getTicketList.selectedTickets
   }
}
const mapDispatchToProps = (dispatch, ownProps) => {

  return {
     onGetSelectedTicket:dispatch(getSelectedTicket())
     
    
  }
}
 export default connect(mapStateToProps,mapDispatchToProps)(MainPage)


