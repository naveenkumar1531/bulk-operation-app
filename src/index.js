import 'react-app-polyfill/ie9';
import 'babel-polyfill';
import {render} from 'react-dom'
import React from 'react'
import { hashHistory} from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import Root from './containers/Root'
import configureStore from './store/configureStore'
const defaultLocale=window.getDefaultLocale();
const messages = "";
try{
messages = window.getTranslations();
} catch(error){
  messages = window.getTranslations();
}
const store = configureStore()
const history = syncHistoryWithStore(hashHistory, store)
render(
  <Root store={store} history={history} defaultLocale={defaultLocale} messages={messages} />,
  document.getElementById('root')
)
