import React, { Component } from 'react'
import {renderRadioButton,renderDynamicField} from './form-elements/FormElements'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import FooterPanel from './Footer'
import {setCustomFields,getCustomFieldsDetails} from '../middlewares/bulkActionMiddleware'

class UpdateTicket extends Component {




  constructor(props) {
    super(props) ;
     this.fields = [
     
      {
        name: 'favouriteColors',
        type: 'select',
        options: [
          { label: 'Red', value: 'red' },
          { label: 'Yellow', value: 'yellow' },
          { label: 'Green', value: 'green' },
        ],
      },
    ];
  }
  componentWillMount(){
    //console.log("inside will mount="+JSON.stringify(this.props.getTicketCustomFields));
    //var customFields = this.props.getTicketCustomFields;
  }
 


 render() {
      
          return <UpdateTicketForm onSubmit={this.props.handleSubmit} fields={this.fields}  customFieldsDetails={this.props.customFieldsDetails} />
  }
}

let UpdateTicketForm =({handleSubmit,fields}) => {
  
  console.log("custom Field details="+JSON.stringify(fields));
  

  var value=true;
  return(
  <form onSubmit={handleSubmit}>
   <div className="bulk-action">
      <div className="bulk-action__content">
        <div id="ticketPriority" className="row">
          <div className="col s12 m12 no-padding">
            <div className="row">
                <div className="col s12 m12 no-padding">
                    <h4>Update Status:</h4>
                  </div>
            </div>
            <div className="row">
              <div className="col s12 no-padding"> 
                     {fields.map(field => (
                      <div key={field.name} >
                        <Field name={field.name}  component={renderDynamicField}  field={field} />
                      </div>
                      ))}
              </div>
            </div>
          </div>
        </div>
     
  </div>
  <FooterPanel/>
  </div>
</form>
   )
}







UpdateTicketForm= reduxForm({
  form: 'customFieldform',                // <------ same form name
  })(UpdateTicketForm)

const mapStateToProps = (state, ownProps) => {
  return {
    customFieldsDetails  : getCustomFieldsDetails(state)
   }
}
const mapDispatchToProps = (dispatch, ownProps) => {
   
  return {

   setTicketCustomFields : dispatch(setCustomFields())
    
  }
}
 export default connect(mapStateToProps,mapDispatchToProps)(UpdateTicket)

 



