import {Input,Row,Autocomplete} from 'react-materialize'
import React from 'react'


const renderTextField = ({input,placeHolder, readonly,onKeyUp}) => {
	 if(readonly==="true"){
	 	return(<input type="text" placeholder={placeHolder} readOnly {...input}/>

		)
	}
	 return(<input type="text" placeholder={placeHolder}  {...input} onKeyUp={onKeyUp}/>

	)	
}
const renderTextFieldInteger =({input,placeHolder, readonly,onKeyUp})=>{
 if(readonly==="true"){
    return(<input type="number" placeholder={placeHolder} readOnly {...input}/>

    )
  }
   return(<input type="number" placeholder={placeHolder}  {...input} onKeyUp={onKeyUp}/>)

}

const renderDynamicField = ({ input, field ,className,s,l,m,children,label}) => {
  const { type, placeholder } = field
  if (type === 'text' || type === 'email' || type === 'number' || type === 'checkbox') {
    return <input type="text" s={s} m={m} l={l}  placeholder={placeholder}  {...input} /> 
  } else if (type === 'select') {
    const { options } = field
    return (
      <select name={field.name} {...input} s={s} m={m} l={l}  onChange={input.onChange}>
        {options.map((option, index) => {
          return <option key={index} value={option.value}>{option.label}</option>
        })}
      </select>
    )
  } else {
    return <div>Type not supported.</div>
  }
}




const renderSelect = ({input,className,s,l,m,children,label}) => (
	<Input type='select' s={s} m={m} l={l} label={label} className={className} children={children}  {...input} onChange={input.onChange}/>
	)

const renderSelectMulti = ({input,className,s,l,m,children,label})=>(<Input type='select' s={s} m={m} l={l} label={label} className={className} children={children}  {...input} onChange={input.onChange} multiple/>)

const renderTextArea = ({input, className,cols,rows,label,meta:{touched, error } }) => (
  <div>
  <textarea  rows={rows} cols={cols}  label={label}  className={className} {...input} ></textarea>{touched && (error && <span>{error}</span>)}
  </div>
			)
const renderCheckBox = ({input, className,label,id}) =>{ 
  return ( 
    <Input type="checkbox"  id={id} value={input.value} className={className} defaultChecked={input.value===true?'checked':''} label={label} onChange={input.onChange}/>
  )}

const renderRadioButton = ({input,id,label,className}) => (
		<Input  type='radio' id={id} value={input.value} className={className} label={label} {...input}  />
	)
const renderAutoSuggest =({input}) =>(
 <Row>
  <Autocomplete
    title='Company'
    data={
      {
        'Apple': null,
        'Microsoft': null,
        'Google': null
      }
    }
  {...input} />
</Row>

	)

module.exports = {
	renderTextField,
	renderSelect,
	renderTextArea,
	renderCheckBox,
	renderRadioButton,
  renderSelectMulti,
	renderAutoSuggest,
  renderTextFieldInteger,
	renderDynamicField
}