import React from 'react'
import FooterPanel from './Footer'
import {FormattedMessage} from 'react-intl';

const CustomFiledNotAllowed = () => {
   return(
    <div className="bulk-action">
      <div className="bulk-action__content">
   		     <div id="requiredCustomFieldMultipleCampaign" className="row">
            <div className="col s12 m12 no-padding custom-field__multiple-campaign">
            <h3><FormattedMessage
                  id='app.messagejsx_header'
                  defaultMessage={"You have selected tickets from multiple campaigns"}
                /></h3>
            <p><FormattedMessage
                  id='app.messagejsx_para'
                  defaultMessage={"Custom field can be updated if you select tickets from the same campaign"}
                /></p>
            </div>
          </div>
          </div>
            <FooterPanel />
            </div>
      

      );
}
 

export default CustomFiledNotAllowed