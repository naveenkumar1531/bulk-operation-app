const DecimalValue = (value) => {
  try{
    var x = parseFloat(value);
    if ( !isNaN(value) ){
      return x
    }
  }catch(e){
    console.log("error in parsing"+value);
  }

}
const IntegerValue =(value) =>{
  try{
  var x = parseFloat(value);
  if ( !isNaN(value) && (x | 0) === x){
    return value
  }
  }catch(e){
    console.log("error in parsing"+value);
  }
}

module.exports = {
  DecimalValue,
  IntegerValue
}