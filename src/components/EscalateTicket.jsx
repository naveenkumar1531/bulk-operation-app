import React, { Component } from 'react'
import {renderRadioButton,renderCheckBox} from './form-elements/FormElements'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import FooterPanel from './Footer'
import {escalateTicket} from '../middlewares/bulkActionMiddleware'
import {FormattedMessage} from 'react-intl';



class EscalateTicket extends Component {

  constructor(props) {
    super(props) 
  }
  componentWillMount(){
  }
 render() {
      
          return <EscalateTicketForm onSubmit={this.props.handleSubmit} />
  }
}
let EscalateTicketForm=props => {
  const{handleSubmit}=props;
  var enabledApplyButton = true;
  var value=true;
  return(
  <form onSubmit={handleSubmit}>
   <div className="bulk-action">
  <div className="bulk-action__content">
    <div id="ticketPriority" className="row">
 <div className="col s12 m12 no-padding">
            <div className="row">
                                <div className="col s12 m12 no-padding">
                                  <h4><FormattedMessage
                  id='app.change_Escalation'
                  defaultMessage={"Change Escalation to:"}
                /></h4>
                                </div>
                                </div>
                                <div className="row">
                                        <div className="col s12 no-padding"> 
                                             <Field name="ticket_escalate" id="Escalate" className="with-gap" component={renderRadioButton} type="radio" value="true" label={<FormattedMessage
                  id='app.escalate'
                  defaultMessage={"Escalate"}
                />} />   
  
                                        </div>
                                        <div className="col s12 no-padding">
                                          
                                          <Field name="ticket_escalate" id="De-Escalate" className="with-gap" component={renderRadioButton} type="radio" value="false" label={<FormattedMessage
                  id='app.de_escalate'
                  defaultMessage={"De-Escalate"}
                />} />   
                                               
                                         
                                        </div>
                       
                                </div>






                        </div>
                </div>
     
  </div>
  <FooterPanel enabledApplyButton = {enabledApplyButton} source="EscalateTicket"/>
  </div>
</form>
   )
}

EscalateTicketForm= reduxForm({
  form: 'escalateform',                // <------ same form name
  initialValues:{ticket_escalate:'true'}
  
  })(EscalateTicketForm)

const mapStateToProps = (state, ownProps) => {
   
  return {
    
   }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  var escalate=(values)=>{
    dispatch(escalateTicket(values));
  }

  return {
    handleSubmit:escalate 
  }
}
 export default connect(mapStateToProps,mapDispatchToProps)(EscalateTicket)

 



