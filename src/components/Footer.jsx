import React from 'react'
import { Link } from 'react-router'
import Loader from './Loader'
import { connect } from 'react-redux'
import { FormattedMessage } from 'react-intl';

const FooterPanel = ({ loader, enabledApplyButton, source }) => {
    var disabled = (enabledApplyButton !== undefined && enabledApplyButton !== false
      ? false : true);
  console.log("in side footer source=", source);
  return (
    <div>
      <div className="bulk-action__footer">
        <Link to="/main"><button id="actionClose" type="button" className="btn btn-light"><FormattedMessage
          id='app.back'
          defaultMessage={"Back"}
        /></button></Link>
        <button type="submit" className="btn btn-primary" disabled={disabled}><FormattedMessage
          id='app.apply'
          defaultMessage={"Apply"}
        /></button>
      </div>
      <Loader isLoaderEnabled={loader} />
    </div>

  );
};
const mapStateToProps = (state, ownProps) => {
  return {
    loader: (state.uiSelection !== undefined) ? (state.uiSelection.loader !== undefined ? state.uiSelection.loader : undefined) : undefined,
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {

  return {



  }
}
export default connect(mapStateToProps, mapDispatchToProps)(FooterPanel)

