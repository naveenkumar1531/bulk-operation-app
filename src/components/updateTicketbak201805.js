

import {setExternalState,setTicketList,setLoaderStatus,setCustomFieldsDetails} from '../actions/bulkAction'
import { push } from 'react-router-redux';
import {  hashHistory, browserHistory} from 'react-router';

export function redirectToMain(){
  return(dispatch,getState) => {
     hashHistory.push('/main');
  }
}
export function closeTicket(values){
	return (dispatch,getState) => {
   var state = getState(); 
   var client = window.AmeyoClient.init();
     if(values.ticket_status === undefined){
      var message =  "Please Select ticket Status";
      var toastNotificationData = {
          type:"error",
          content:message
      }
      client.interface.trigger("toastNotification", toastNotificationData).then(successCallback).catch(failureCallback);
       return; 
    }
     if(values.comment === undefined){
      var message =  "Please Enter Closure Reason";
      var toastNotificationData = {
          type:"error",
          content:message
      }
      client.interface.trigger("toastNotification", toastNotificationData).then(successCallback).catch(failureCallback);
       return; 
    }
    var successT = 0;
		var failureT = 0;
		var totalT = 0;
    var ticketNotAssdinedFailureCount=[];
    var uiValidationFailed =[];
    var othersValidationFailed=[];
    var ticketList = (state.getTicketList.selectedTickets !==undefined?state.getTicketList.selectedTickets:'');
    if(ticketList !=='')
    {
      dispatch(setLoaderStatus(true));
      var length = ticketList.length;
  			 totalT = length;
    		for (var i = 0; i < length; i++) {
   				var updateObject={}
   				updateObject["subject"]=ticketList[i].subject;
       		updateObject["ticketId"] = ticketList[i].ticketId;
       		updateObject["externalState"] = values.ticket_status;
       		updateObject["closureReason"]=values.comment;		
       	  client.contextData.update("ticket", updateObject).then((successData)=>{
			     	successT++;
           	totalT--;
           	if (totalT == 0) {
              dispatch(setLoaderStatus(false));
              sendSuccessMessage(successT);
              if(failureT != '0'){
                //sendFailureMessage(failureT,ticketNotAssdinedFailureCount,uiValidationFailed,othersValidationFailed)
                sendFailureMessage(failureT,uiValidationFailed) 
              }
              dispatch(redirectToMain());  
				      }
       			 }).catch((failureData)=>{
              // console.log('failure'+JSON.stringify(failureData));
                uiValidationFailed.push(failureData);
               failureT++;
           		 totalT--;
                
               	if (totalT == 0) {
                  dispatch(setLoaderStatus(false));
                 
                  if(successT != '0'){
                    sendSuccessMessage(successT);  
                  }
                  
                 // sendFailureMessage(failureT,ticketNotAssdinedFailureCount,uiValidationFailed,othersValidationFailed)  
                  sendFailureMessage(failureT,uiValidationFailed)  
                  dispatch(redirectToMain());                
                }
              }
       			);
			  }	
     }
   }
}

export function changeStatus(values){
  return (dispatch,getState) => {
   
    var state = getState(); 
   
    var client = window.AmeyoClient.init();
     if(values.ticket_status === undefined){
      var message =  "Please Select ticket Status";
      var toastNotificationData = {
          type:"success",
          content:message
      }
      client.interface.trigger("toastNotification", toastNotificationData).then(successCallback).catch(failureCallback);
       return; 
    }

    var successT = 0;
    var failureT = 0;
    var totalT = 0;
    var ticketNotAssdinedFailureCount=[];
    var uiValidationFailed =[];
    var othersValidationFailed=[];
    
    //client.data.getList("selectedTickets",(ticketList)=>
    var ticketList = (state.getTicketList.selectedTickets !==undefined?state.getTicketList.selectedTickets:'');
    if(ticketList !=='')
    {
      dispatch(setLoaderStatus(true));
      var length = ticketList.length;
        totalT = length;
        for (var i = 0; i < length; i++) {
          var updateObject={}
          updateObject["subject"]=ticketList[i].subject;
            updateObject["ticketId"] = ticketList[i].ticketId;
            updateObject["externalState"] = values.ticket_status;
            client.contextData.update("ticket", updateObject).then ((successData)=>{
            successT++;
              totalT--;
              if (totalT == 0) {
                dispatch(setLoaderStatus(false));
               sendSuccessMessage(successT);
               if(failureT != '0'){
               //sendFailureMessage(failureT,ticketNotAssdinedFailureCount,uiValidationFailed,othersValidationFailed)
               sendFailureMessage(failureT,uiValidationFailed) ;
                }
                dispatch(redirectToMain());
               
              }
              
             }).catch((failureData)=>{
                uiValidationFailed.push(failureData);
           
               failureT++;
               totalT--;
            if (totalT == 0) {
                  dispatch(setLoaderStatus(false));
                   sendFailureMessage(failureT,uiValidationFailed) ;
                   if(successT != '0'){
                   sendSuccessMessage(successT) 
                   }  
                  dispatch(redirectToMain());

                }
              }
            );
      } 
     }
   }
}

export function changeTicketPriority(values){
  return (dispatch,getState) => {
   var state = getState(); 
   var client = window.AmeyoClient.init();
   var successT = 0;
   var failureT = 0;
   var totalT = 0;
   var ticketNotAssdinedFailureCount=[];
   var uiValidationFailed =[];
   var othersValidationFailed=[];
   var ticketList = (state.getTicketList.selectedTickets !==undefined?state.getTicketList.selectedTickets:'');
    if(ticketList !=='')
    {
      dispatch(setLoaderStatus(true));
      var length = ticketList.length;
         totalT = length;
        for (var i = 0; i < length; i++) {
          var updateObject={}
          updateObject["subject"]=ticketList[i].subject;
          updateObject["ticketId"] = ticketList[i].ticketId;
          updateObject["priority"] = values.ticket_priority;
          client.contextData.update("ticket", updateObject).then((successData)=>{
            successT++;
            totalT--;
            if (totalT == 0) {
                dispatch(setLoaderStatus(false));
                sendSuccessMessage(successT);
                if(failureT != '0'){
                  //sendFailureMessage(failureT,ticketNotAssdinedFailureCount,uiValidationFailed,othersValidationFailed)
                  sendFailureMessage(failureT,uiValidationFailed) ;
                }
                 dispatch(redirectToMain());
            }
          }).catch ((failureData)=>{
            uiValidationFailed.push(failureData);
          
            failureT++;
            totalT--;
            if (totalT == 0) {
                  dispatch(setLoaderStatus(false));
                   //sendFailureMessage(failureT,ticketNotAssdinedFailureCount,uiValidationFailed,othersValidationFailed)
                   sendFailureMessage(failureT,uiValidationFailed) 
                   if(successT != '0'){
                   sendSuccessMessage(successT) 
                   }  
                  dispatch(redirectToMain());

                }
              }
            );
         } 
     }
   }
}

export function escalateTicket(values){
  return (dispatch,getState) => {
   var state = getState(); 
   var client = window.AmeyoClient.init();
   var successT = 0;
   var failureT = 0;
   var totalT = 0;
   var ticketNotAssdinedFailureCount=[];
   var uiValidationFailed =[];
   var othersValidationFailed=[];
   var ticketList = (state.getTicketList.selectedTickets !==undefined?state.getTicketList.selectedTickets:'');
    if(ticketList !=='')
    {
      dispatch(setLoaderStatus(true));
      var length = ticketList.length;
      totalT = length;
      if(values.ticket_escalate==="true"){
            var isEscalate= true;
      } else{
            var isEscalate= false;
      }
      for (var i = 0; i < length; i++) {
          var updateObject={}
          updateObject["subject"]=ticketList[i].subject;
          updateObject["ticketId"] = ticketList[i].ticketId;
          updateObject["escalate"] = isEscalate;
          client.contextData.update("ticket", updateObject).then((successData)=>{
            successT++;
            totalT--;
            if (totalT == 0) {
                dispatch(setLoaderStatus(false));
                sendSuccessMessage(successT);
                if(failureT != '0'){
                  sendFailureMessage(failureT,uiValidationFailed) 
                }
                 dispatch(redirectToMain());
            }
          }).catch( ( failureData )=>{
            uiValidationFailed.push(failureData);
            
            
            failureT++;
            totalT--;
            if (totalT == 0) {
                  dispatch(setLoaderStatus(false));
                   //sendFailureMessage(failureT,ticketNotAssdinedFailureCount,uiValidationFailed,othersValidationFailed)
                   sendFailureMessage(failureT,uiValidationFailed) ;
                   if(successT != '0'){
                   sendSuccessMessage(successT) 
                   }  
                  dispatch(redirectToMain());

                }
              }
            );
         } 
     }
   }
}

export function setCustomFields(values){
  return (dispatch,getState) => {
   var state = getState(); 
   var client = window.AmeyoClient.init();
   client.globalData.get("customFields") 
    .then( (successCallback)  => { 
      //console.log(" update response="+JSON.stringify(successCallback))
      dispatch(setCustomFieldsDetails(successCallback));
      //console.log(" update after response="+JSON.stringify(successCallback)) })
    .catch( (failureCallback)  => {
      console.log("failure response="+JSON.stringify(failureCallback)) 
      }
    )
  }
}

export function updateCustomFields(values){
  return (dispatch,getState) => {
   var state = getState();
   console.log("value",values.customFields);
   var ticketNotAssdinedFailureCount=[];
   var successT = 0;
   var failureT = 0;
   var totalT = 0;
   var uiValidationFailed =[];
   var othersValidationFailed=[];
   var ticketList = (state.getTicketList.selectedTickets !==undefined?state.getTicketList.selectedTickets:'');
   var form = (state.form !==undefined?state.form.customFieldform:'');
   var formCustomField  = (form.values!==undefined? form.values.customFields:'');
    if (formCustomField!=''){
      var stateCustomFields = (state.getTicketList.customFields!==undefined?state.getTicketList.customFields:'');
      var selectedCustomFieldDataTypeVar = selectedCustomFieldDataType(stateCustomFields,formCustomField);
      var selectedCustomTypeVar   =selectedCustomType(stateCustomFields,formCustomField);
      //console.log("customFieldDataType",selectedCustomFieldDataTypeVar,"selectedCustomTypeVar",selectedCustomTypeVar);  
      //console.log("formCustomFieldValue",formCustomFieldValue,"type of ",typeof formCustomFieldValue);
      //if (formCustomFieldValue){
        var customFields = {};
        var tempVar=[];
        if (selectedCustomTypeVar=='CheckBox'){
          var formCustomFieldValue  = ( form.registeredFields!==undefined? form['registeredFields']:undefined);
          if (formCustomFieldValue!=undefined){
              for(var valuesKey in formCustomFieldValue){
                if (selectedCustomFieldDataTypeVar=='Integer' || selectedCustomFieldDataTypeVar=='Decimal'){
                    if (valuesKey!='customFields'){
                      tempVar.push(parseInt(valuesKey));
                      }
                }else {
                  if (valuesKey!='customFields'){
                    tempVar.push(valuesKey);
                  }
                }
            }
            formCustomFieldValue=tempVar;
          }
        }else {
        var formCustomFieldValue  = ( form.values!==undefined? form['values'][formCustomField]:undefined);
        if (typeof formCustomFieldValue==='object'){
          if (selectedCustomFieldDataTypeVar=='Integer' || selectedCustomFieldDataTypeVar=='Decimal'){
              for (var index in formCustomFieldValue){
                tempVar.push(parseInt(formCustomFieldValue[index]));
              }
              formCustomFieldValue=tempVar;
              //console.log("tempVar=",tempVar);
          }
        }else {
          formCustomFieldValue=(selectedCustomFieldDataTypeVar=='Integer' || selectedCustomFieldDataTypeVar=='Decimal' ?parseInt(formCustomFieldValue):formCustomFieldValue);
        }
      }
        customFields[formCustomField]=formCustomFieldValue;
     //}
    //console.log("customFields",customFields);
    if(ticketList !=='')
    {
      dispatch(setLoaderStatus(true));
      var length = ticketList.length;
      var totalT = length;
      for (var i = 0; i < length; i++) {
          var updateObject={}
          updateObject["subject"]=ticketList[i].subject;
          updateObject["ticketId"] = ticketList[i].ticketId;
          updateObject["customFields"]=customFields;
          var client = window.AmeyoClient.init();
          client.contextData.update("ticket", updateObject).then((successData)=>{ 
            console.log("bulk success",successData)
             successT++;
            totalT--;
            if (totalT == 0) {
                dispatch(setLoaderStatus(false));
                sendSuccessMessage(successT);
                if(failureT != '0'){
                 sendFailureMessage(failureT,uiValidationFailed) ;
                }
                 dispatch(redirectToMain());
            }  

          }).catch((failureData)=>{ 
              uiValidationFailed.push(failureData);
              
            failureT++;
            totalT--;
            if (totalT == 0) {
                  dispatch(setLoaderStatus(false));
                   sendFailureMessage(failureT,uiValidationFailed) ;
                   if(successT != '0'){
                   sendSuccessMessage(successT) 
                   }  
                  dispatch(redirectToMain());

                }
              });
          //console.log("final data array=",updateObject);   
      }
      dispatch(redirectToMain());
    }
  }
  //console.log("insdie sub",state);
  }
}
export function getCustomFieldsDetails(state){
  console.log("fetching state",state);
  var campaignCustomFields={};
  var allCustomFields = state.getTicketList === undefined ? undefined : state.getTicketList.customFields ;
  if (allCustomFields!=undefined){
    for (var key in allCustomFields){
    campaignCustomFields = allCustomFields[key]
    break;
  }
  }
  return campaignCustomFields;
}
function selectedCustomFieldDataType (customFieldsDetails,customFieldsValue) {
  console.log("customFieldsDetails",customFieldsDetails,"customFieldsValue",customFieldsValue);
  for (var key in customFieldsDetails) {
    for (var innerkey in customFieldsDetails[key]){
            //console.log("outer loop custom field=",customFieldsDetails[key]['customFieldId']);
            console.log("inside inner looop custom field=",customFieldsDetails[key][innerkey].customFieldId);
          if (customFieldsDetails[key][innerkey].customFieldId==customFieldsValue){
            //console.log("selected custom field=",customFieldsDetails[key]);
            return customFieldsDetails[key][innerkey].customFieldDataType;
          }
        
      }
 }
}
function selectedCustomType (customFieldsDetails,customFieldsValue) {
  console.log("customFieldsDetails",customFieldsDetails,"customFieldsValue",customFieldsValue);
  for (var key in customFieldsDetails) {
    for (var innerkey in customFieldsDetails[key]){
            //console.log("outer loop custom field=",customFieldsDetails[key]['customFieldId']);
            console.log("inside inner looop custom field=",customFieldsDetails[key][innerkey].customFieldId);
          if (customFieldsDetails[key][innerkey].customFieldId==customFieldsValue){
            //console.log("selected custom field=",customFieldsDetails[key]);
            return customFieldsDetails[key][innerkey].customFieldType;
          }
        
      }
 }
}


function sendSuccessMessage(ticketCount){
   var client = window.AmeyoClient.init();
   var message =  ticketCount + "  tickets updated successfully";
      var toastNotificationData = {
          type:"success",
          content:message
      }
      client.interface.trigger("toastNotification", toastNotificationData).then(successCallback).catch(failureCallback);
                 
  

}

function sendFailureMessage (ticketCount,failureData){
  var client = window.AmeyoClient.init();
    var failure100=[];

    var failure101=[];
    var failure102=[];
    var failure121=[];
    var failure122=[];
    var failure123=[];
    var failure124=[];
    var failure125=[];
    var failure126=[];
    var failure127=[];
    var m1='';

  for ( var key in failureData ){

          if(failureData[key].code=='100')
              failure100.push(failureData.ticketId);
          else if (failureData[key].code=='101')
              failure101.push(failureData.ticketId);
          else if (failureData[key].code=='102')
              failure102.push(failureData.ticketId);
          else if (failureData[key].code=='121')
              failure121.push(failureData.ticketId);
          else if (failureData[key].code=='122')
              failure122.push(failureData.ticketId);
          else if (failureData[key].code=='123')
              failure123.push(failureData.ticketId);
          else if (failureData[key].code=='124')
              failure124.push(failureData.ticketId);
          else if (failureData[key].code=='125')
              failure125.push(failureData.ticketId);
          else if (failureData[key].code=='126')
              failure126.push(failureData.ticketId);
          else if (failureData[key].code=='127')
              failure127.push(failureData.ticketId);
          
    }
if(failure100.length>0){
  m1 += failure100.length + "sdk request timeout"
}
if(failure101.length>0){
  m1 += failure101.length + " invalid input provided by app"
}
if(failure102.length>0){
  m1 += failure102.length + "  internal failure"
}
if(failure121.length>0){
  m1 += failure121.length + " requested data object does not exists"
}
if(failure122.length>0){
  m1 += failure122.length + " action does not exist"
}
if(failure123.length>0){
  m1 += failure123.length + " requested trigger operation does not exist"
}
if(failure124.length>0){
  m1 += failure124.length + " provided ticket id not selected"
}
if(failure125.length>0){
  m1 += failure125.length + " tickets are not assigned to user"
}
if(failure126.length>0){
  m1 += failure126.length + " provided ticket id already assigned to logged in user"
}
if(failure127.length>0){
  m1+= failure127.length + " provided ticket already in the assigned state"
}
var message =  "Unable to update "+ ticketCount + " tickets in which " + m1;
      var toastNotificationData = {
          type:"error",
          content:message
      }
      client.interface.trigger("toastNotification", toastNotificationData).then(successCallback).catch(failureCallback);
}


export function getExternalState(){
	return (dispatch) => {
			var client = window.AmeyoClient.init();
			client.globalData.get("ticketStates").then((success)=>{
					dispatch(setExternalState(success))
			}).catch((failure)=>{
        var toastNotificationData = {
          type:"failure",
          content:"Not able to get external state"
      }
      client.interface.trigger("toastNotification", toastNotificationData).then(successCallback).catch(failureCallback);
				
			});

		}
			
}
function successCallback(){
  //console.log('success');
}
function failureCallback(failure){
   console.log('failure'+JSON.stringify(failure));
}
export const getTicketState = (state) =>
 {
 	var externalStates = (state.uiSelection !== undefined?(state.uiSelection.externalStates!==undefined?(state.uiSelection.externalStates):undefined):undefined)
 	
	return {
		externalStates
	}
}
export const getTicketCount = (state) =>
 {

  var ticketCount = (state.getTicketList !== undefined?(state.getTicketList.selectedTickets!==undefined?(state.getTicketList.selectedTickets):undefined):undefined)
 // console.log('dddddd'+JSON.stringify(ticketCount));
  if(ticketCount !==undefined){
    ticketCount = ticketCount.length;
  }
  return {
    ticketCount
  }
}


export function getSelectedTicket(){
  return (dispatch,getState) => {
    var state = getState();
    var isTicketSelected = (state.getTicketList !== undefined?(state.getTicketList.selectedTickets!==undefined?(state.getTicketList.selectedTickets):undefined):undefined);
    if(isTicketSelected === undefined){
      var client = window.AmeyoClient.init();
      client.contextData.get("selectedTickets").then ((ticketList)=>{

      //client.data.getList("selectedTickets", (ticketList)=>{
          dispatch(setTicketList(ticketList))
      }).catch( (failure)=>{
        
       // client.ui.trigger("notification", { type: "error", content: "Failed to fetch Ticket List"});
      });

    }
  
  }
}


