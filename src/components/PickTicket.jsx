import React, { Component } from 'react'
import { renderRadioButton, } from './form-elements/FormElements'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import FooterPanel from './Footer'
import { FormattedMessage } from 'react-intl';
import { pickTicket } from '../middlewares/bulkActionMiddleware'

var enabledApplyButton = false;
class PickTicket extends Component {

  constructor(props) {
    var messagePart1 = '';
    var messagePart2 = '';
    super(props)
  }


  componentWillMount() {
    this.props.pickTicket;
    var ticketList = this.props.selectedTicketsList;
    //var ableToPick = checkTicketStatus(selectedTicketsList);
    var ableToPick = true;
    for (var key in ticketList) {
      if (ticketList[key].assignedUserId != null) {
        ableToPick = false;
        break;
      }
    }
    enabledApplyButton = (ableToPick == true ? true : false)
    if (ableToPick) {
      this.messagePart1 = ticketList.length == 1 ? <FormattedMessage
        id='app.ticket'
        defaultMessage={"Ticket"}
      /> : <FormattedMessage
          id='app.tickets'
          defaultMessage={"Tickets"}
        />;
      this.messagePart2 = <FormattedMessage
        id='app.assgined_to_you'
        defaultMessage={"wiil be assigned to you"}
      />
    }
    else {
      this.messagePart1 = <FormattedMessage
        id='app.unassigned_can_be_picked'
        defaultMessage={"Only the tickets that are unassigned can be picked"}
      />
    }
  }

  render() {

    return <PickTicketForm messagePart1={this.messagePart1} messagePart2={this.messagePart2} onSubmit={this.props.handleSubmit} setButtonStatusData={this.props.setButtonStatusData} selectedTicketsList={this.props.selectedTicketsList} />
  }
}
let PickTicketForm = props => {
  const { handleSubmit, selectedTicketsList, setButtonStatusData, messagePart1, messagePart2 } = props;
  //console.log("message this="+message);
  var value = true;
  return (
    <form onSubmit={handleSubmit}>
      <div className="bulk-action">
        <div className="bulk-action__content">


          <div id="requiredCustomFieldMultipleCampaign" className="row notification-msg">
            <div className="col s12 m12 no-padding custom-field__multiple-campaign">
              <h3>{messagePart1}{" "}{messagePart2}</h3>

            </div>
          </div>

        </div>

        <FooterPanel enabledApplyButton={enabledApplyButton} source="PickTicket"/>
      </div>
    </form>
  )
}

PickTicketForm = reduxForm({
  form: 'pickTicketForm',                // <------ same form name
  initialValues: { ticket_escalate: 'true' }

})(PickTicketForm)

const mapStateToProps = (state, ownProps) => {
  return {
    selectedTicketsList: state.getTicketList === undefined ? '' : state.getTicketList.selectedTickets,
    buttonType: (state.uiSelection !== undefined) ? state.uiSelection.buttonType !== undefined ? state.uiSelection.buttonType : '' : ''
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  var callPickTicket = (values) => {
    dispatch(pickTicket(values));

  }
  
  var callHandleSubmit = (values) => { dispatch(pickTicket(values)) }

  return {
    pickTicket: callPickTicket,
    handleSubmit: callHandleSubmit
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(PickTicket)





