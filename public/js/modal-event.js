var client = AmeyoClient.init();
client.lifeCycle.register("appUnload", appUnloadCallback);

function appUnloadCallback(){
	client.contextData.trigger("refresh","ticketList").then(successCallback).catch(failureCallback);	
}
function successCallback(){
  console.log('success');
}
function failureCallback(failure){
   console.log('failure'+JSON.stringify(failure));
}

