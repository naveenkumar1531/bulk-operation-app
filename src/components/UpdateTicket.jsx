import React, { Component } from 'react'
import {
  renderRadioButton,
  renderSelect,
  
} from './form-elements/FormElements'

import {  
  SingleLineField,
  MultiLineField,
  CheckBox,
  RadioButton,
  SingleSelectionListBox,
  MultiSelectionListBox,
  DependentSingleSelectionListBox,
  dependentField,
  DateField,
  DefaultField } from './form-elements/CustomFieldElements'

import { Field, reduxForm,formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import FooterPanel from './Footer'
import CustomFiledNotAllowed from './Message'
import {setCustomFields,getCustomFieldsDetails,updateCustomFields} from '../middlewares/bulkActionMiddleware'
import {FormattedMessage} from 'react-intl';

class UpdateTicket extends Component {

  constructor(props) {
    super(props) ;
  }
  componentWillMount(){
    //console.log("inside will mount="+JSON.stringify(this.props.getTicketCustomFields));
    this.props.setTicketCustomFields;

  }

 render() {   
          return <UpdateTicketForm fields={this.fields} onSubmit={this.props.handleSubmit}  customFieldsValue={this.props.customFieldsValue} 
          customFieldsDetails={this.props.customFieldsDetails}
          selectedTicketsCampaign={this.props.selectedTicketsCampaign}  customFields = {this.props.customFields}/>
  }
}

let UpdateTicketForm = props  => {
  const{handleSubmit, customFieldsDetails,customFieldsValue,
    fields,selectedTicketsCampaign,customFields
   } = props;
   
function selectedCustomField (customFieldsValue,customFieldsDetails) {
  //console.log(Object.keys(customFieldsDetails).length,"customFieldsValue",customFieldsValue);
  for (var key in customFieldsDetails) {
          if (customFieldsDetails[key]['customFieldId']==customFieldsValue){
            return customFieldsDetails[key];
          }
      }
 }
 function designSelectedField(selectedCustomFieldObj){
  var customFieldType= selectedCustomFieldObj['customFieldType'];
  var customFieldName= selectedCustomFieldObj['customFieldName'];
  var customFieldId= selectedCustomFieldObj['customFieldId'];
  //customFieldType='RadioButton';
  //console.log("gaurav customFieldType",customFieldType);
  var field;
  var html=[];
  switch (customFieldType){
    case 'SingleLineField':
          html.push(<SingleLineField  selectedCustomFieldObj = {selectedCustomFieldObj} key={customFieldName}/>);
            return html;
    case 'MultiLineField':
          html.push(<MultiLineField  selectedCustomFieldObj = {selectedCustomFieldObj} key={customFieldName} />);
            return html;
    case 'CheckBox':
          console.log("CheckBox");
          html.push(<CheckBox  selectedCustomFieldObj = {selectedCustomFieldObj}  key={customFieldId}/>);
            return html;
    case 'RadioButton':
          html.push(<RadioButton  selectedCustomFieldObj = {selectedCustomFieldObj} key={customFieldId}/>);
            return html;
    case 'SingleSelectionListBox':
            html.push(<SingleSelectionListBox selectedCustomFieldObj = {selectedCustomFieldObj} key={customFieldId} />);
            return html;
    case 'MultiSelectionListBox':
            html.push(<MultiSelectionListBox  selectedCustomFieldObj = {selectedCustomFieldObj} key={customFieldName}/>);
            return html;
    // case 'DependentSingleSelectionListBox':
    //         html.push(<DependentSingleSelectionListBox  selectedCustomFieldObj = {selectedCustomFieldObj} key={customFieldName}/>);
    //         return html;
    case 'DateField':
             html.push(<DateField  selectedCustomFieldObj = {selectedCustomFieldObj} key={customFieldName}/>);
             return html;
    default:
            html.push(<div><FormattedMessage
              id='app.update_ticket_html'
              defaultMessage={"Selected Field Type Not Supported by this App"}
            /> </div>);
            return html;
  }
  }
   if (customFieldsValue){
    var selectedCustomFieldObj = selectedCustomField(customFieldsValue,customFieldsDetails);
    var designField = designSelectedField(selectedCustomFieldObj);
  }
    var options=[];
    var dependentData=[];
      if (customFieldsDetails!==undefined){
      var options=[];
       options.push(<FormattedMessage id='app.selectOption' defaultMessage={"Select Option"}>
            {(message) => <option value="" key="1">{message}</option>}
          </FormattedMessage>);
      for(let j=0;j< customFieldsDetails.length;j++){
          if (customFieldsDetails[j]['customFieldType']=='DateField' || customFieldsDetails[j]['customFieldType']=='DependentSingleSelectionListBox'){
            continue;
          }else{
            options.push(<option 
            value={customFieldsDetails[j]['customFieldId']} 
            key={customFieldsDetails[j]['customFieldCategoryId']} > 
            {customFieldsDetails[j]['customFieldName']}
            </option>)
          }
        }
  }
  
  if(selectedTicketsCampaign !== undefined && selectedTicketsCampaign.length > 1){
    return (
      <CustomFiledNotAllowed/>
    )
  } else {
   return(
   <form onSubmit={handleSubmit}>
   <div className="bulk-action">
      <div className="bulk-action__content">
        <div  className="row">
          <div className="col s12 m12 no-padding">
          <div className="row">
            <div className="col s12 m12 no-padding">
              <h4><FormattedMessage
                  id='app.select_Custom_Field'
                  defaultMessage={"Select Custom Field:"}
                /></h4>
            </div>
            </div>
            <div className="row">
              <div className="col s12 no-padding update-custom-field"> 
                <div className="row select">
                  <Field name="customFields"  component={renderSelect}  label="" s={6} m={6}>
                    {options}
                  </Field>
                </div>
              </div>
            </div>
             <div className="row">
              <div className="col 12">
              <p></p>
              </div>
            </div>
            <div className="row">
              {designField}
            </div>
    </div>
  </div>  
  </div>
  <FooterPanel enabledApplyButton = {customFields} source ={"UpdateTicket"}/>
  </div>
</form>
   )
  }
}

UpdateTicketForm= reduxForm({
  form: 'customFieldform',                // <------ same form name
  })(UpdateTicketForm)

const selector = formValueSelector('customFieldform') 
UpdateTicket = connect(
  state => ({
    customFields:state.form.customFieldform !== undefined ? (state.form.customFieldform.values !== undefined ?
      (state.form.customFieldform.values.customFields !== undefined ?
        (state.form.customFieldform.values.customFields !== " " ? state.form.customFieldform.values.customFields :
        false) : false) : false) : false

  }))(UpdateTicket)

const mapStateToProps = (state, ownProps) => {
  return {
    customFieldsDetails  : getCustomFieldsDetails(state),
    customFieldsValue : selector(state, 'customFields'),
    selectedTicketsCampaign : state.getTicketList === undefined ? '' : state.getTicketList.selectedTicketsCampaign
   }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  var updateField=(values)=>{
        dispatch(updateCustomFields(values));
  }
 
  return {
    handleSubmit          : updateField,
    setTicketCustomFields : dispatch(setCustomFields())    
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(UpdateTicket)

 



